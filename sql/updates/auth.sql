/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50529
Source Host           : localhost:3306
Source Database       : auth

Target Server Type    : MYSQL
Target Server Version : 50529
File Encoding         : 65001

Date: 2013-04-10 00:46:22
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `account`
-- ----------------------------
DROP TABLE IF EXISTS `account`;
CREATE TABLE `account` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Identifier',
  `username` varchar(32) NOT NULL DEFAULT '',
  `sha_pass_hash` varchar(40) NOT NULL DEFAULT '',
  `sessionkey` varchar(80) NOT NULL DEFAULT '',
  `v` varchar(64) NOT NULL DEFAULT '',
  `s` varchar(64) NOT NULL DEFAULT '',
  `email` varchar(254) NOT NULL DEFAULT '',
  `joindate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_ip` varchar(15) NOT NULL DEFAULT '127.0.0.1',
  `failed_logins` int(10) unsigned NOT NULL DEFAULT '0',
  `locked` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `last_login` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `online` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `expansion` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `mutetime` bigint(20) NOT NULL DEFAULT '0',
  `mutereason` varchar(255) NOT NULL DEFAULT '',
  `muteby` varchar(50) NOT NULL DEFAULT '',
  `locale` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `os` varchar(3) NOT NULL DEFAULT '',
  `recruiter` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COMMENT='Account System';

-- ----------------------------
-- Records of account
-- ----------------------------
INSERT INTO `account` VALUES ('1', 'WOW1', '7F537C66B65FEF5656AD19A55DEF310397BED826', '', '82E2B5AB5D2E7F0C60ED68AD7B32ABB8CC3296AC65E7B640F2AC9EE90D85AFBB', '91376261040B27BF5F96DE4D9D0F6E48FE35A450B6432CD4A7A1B99043ED700B', '', '2012-11-06 00:44:50', '127.0.0.1', '0', '0', '2013-01-13 00:45:45', '0', '2', '0', '', '', '8', 'Win', '0');
INSERT INTO `account` VALUES ('2', 'WOW2', 'FA1D95A914654F8BD19C24A6E4380BE6914344AE', '70F81BA3AF2AE9C4323ACE7AED8FA22869D3F1BFCC8418D58A51DC83691F062C9AFD01B4BA800BA8', '6864004B8E80CA3083C727BF5BAFF3E1EEB1DA1D8E7575BB080BD63EAEC6C442', 'F68A6CB08B95D8C004C9646389A9253DFC16F705C2979262AAC634F6A2184669', '', '2012-11-30 02:44:32', '127.0.0.1', '0', '0', '2013-04-09 00:22:51', '0', '2', '0', '', '', '8', 'Win', '0');
INSERT INTO `account` VALUES ('3', 'WOW3', '3D068872C0C502F0548F3AC6E819AB9136AE6684', 'F29EFF4A4D9752010848DBC89384DD56F580AFC3A553BFEF71090A25FC5645797C44596B3A9604A8', '3D20C23FF42EAFAD4D55334FB368D5249E3F56154428501CA52175C1D27F0818', '8D399A9959AFF0B7B4FD8DE76F57FB8AF7FE50573A1D0B0C241244EC24597041', '', '2012-11-30 02:44:48', '127.0.0.1', '0', '0', '2013-04-05 21:08:01', '0', '2', '0', '', '', '8', 'Win', '0');
INSERT INTO `account` VALUES ('4', 'WOWW', 'BD9254216A74AE746E1172B4B71D3232F56B5A8E', '', '1DEE93DEF8C01E5BB5F6B4172FA61C35669A41071FDFEDCBE5963759760E0DE7', '80DCB309D74E1F5276F1582C2F01AFDD40714D431982546C8F27568C5AAFC377', '', '2012-12-06 20:23:46', '127.0.0.1', '0', '0', '2012-12-26 20:40:19', '0', '2', '0', '', '', '8', 'Win', '0');
INSERT INTO `account` VALUES ('5', 'WOWW2', 'D0B3F987AFDD406E7E1D1A560CAFE9094CADA9D8', '', '', '', '', '2012-12-06 20:29:01', '127.0.0.1', '0', '0', '0000-00-00 00:00:00', '0', '2', '0', '', '', '0', '', '0');
INSERT INTO `account` VALUES ('6', 'MANYACHOK', 'D1637718D95D856D540DFD12DFA6AAC77568AA68', '', '1A0598CD877CCC8B3C59625AD0940DC75F9D8480676027BD44EFD089459180E1', 'BAB9C9A76077E3875AB73F285301CF20F4164BC5EFB49CD0949DEA6DFF3A7A11', '', '2012-12-19 00:50:18', '178.127.6.58', '0', '0', '2013-02-06 21:02:10', '0', '2', '0', '', '', '8', 'Win', '0');
INSERT INTO `account` VALUES ('7', 'vip', '37ab928d6e1bd740ecc97159671224a653b9dcd9', '', '', '', 'vip@m.ru', '2012-12-19 16:30:53', '127.0.0.1', '0', '0', '0000-00-00 00:00:00', '0', '2', '0', '', '', '0', '', '0');
INSERT INTO `account` VALUES ('8', 'WOW5', 'FCADE39741F85E802DB414471438EE60CD3E4130', '', '', '', '', '2012-12-23 18:12:09', '127.0.0.1', '0', '0', '0000-00-00 00:00:00', '0', '2', '0', '', '', '0', '', '0');
INSERT INTO `account` VALUES ('9', 'LIMER', '1AE14628C175A3202192890C56F45E89DA27C698', '', '791144D674654BECB94ECE22C520A1D623316B8A781B6835F47CA35F950894E5', '98BFD32E248002D0369CFBCBF8AA1B954B1CB12C970A53DC48970BB453128EA1', '', '2012-12-23 18:14:04', '95.83.108.122', '0', '0', '2012-12-23 18:18:40', '0', '2', '0', '', '', '8', 'Win', '0');
INSERT INTO `account` VALUES ('10', 'WOW4', 'BDC81DD137773783A38902B3B2FC34D21D397A91', 'A7D18A8603499D1B1E2F3D06DA4F160014B02F0F5FF94C58B251FC25343EA50233B06D77C2923E3D', '70B862FFED17EA305558C6DE42D5176BDCFE8AD360F5595CAF931A66E894FBEF', '815449DC0FA1BB0E84AE06A093550C526EA7BB3C03BE36A4695A58A4C6241071', '', '2012-12-30 03:23:32', '127.0.0.1', '0', '0', '2013-03-03 18:47:20', '0', '2', '0', '', '', '8', 'Win', '0');
INSERT INTO `account` VALUES ('11', 'LIINA', '4D40F618DFAC4B6E2EE4BD544CC7CE6DBB77E04F', '', '2865CC9ADC1F86EE8573A94DA6D7084797BB02C926983DB5FB5A0449DA16C121', 'A4C2F1FDBF74CB79F642C67D409E434477A23A81ED2B6418ED6BF6CB75DEB7AF', '', '2013-02-02 23:47:00', '31.129.123.137', '0', '0', '2013-02-06 18:42:01', '0', '2', '0', '', '', '8', 'Win', '0');
INSERT INTO `account` VALUES ('12', 'QWERTY', '29E4B96FA73BB9DE02DB0F402C6C7F898B4880D7', '', '292FC40994E3BA798FC07382B96F9E36A4CCC9995E85A9E8A68302EA8B514408', 'CEAB4831C66A8E421CA46C8F0781254ACD15DD929F6BEB7979FA1A71A9A7D595', '', '2013-02-02 23:47:49', '109.254.70.192', '0', '0', '2013-02-06 15:55:07', '0', '2', '0', '', '', '8', 'Win', '0');

-- ----------------------------
-- Table structure for `account_access`
-- ----------------------------
DROP TABLE IF EXISTS `account_access`;
CREATE TABLE `account_access` (
  `id` int(10) unsigned NOT NULL,
  `gmlevel` tinyint(3) unsigned NOT NULL,
  `RealmID` int(11) NOT NULL DEFAULT '-1',
  PRIMARY KEY (`id`,`RealmID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of account_access
-- ----------------------------
INSERT INTO `account_access` VALUES ('2', '3', '-1');
INSERT INTO `account_access` VALUES ('3', '3', '1');
INSERT INTO `account_access` VALUES ('6', '4', '-1');
INSERT INTO `account_access` VALUES ('9', '4', '-1');
INSERT INTO `account_access` VALUES ('11', '4', '-1');
INSERT INTO `account_access` VALUES ('12', '3', '-1');

-- ----------------------------
-- Table structure for `account_banned`
-- ----------------------------
DROP TABLE IF EXISTS `account_banned`;
CREATE TABLE `account_banned` (
  `id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Account id',
  `bandate` int(10) unsigned NOT NULL DEFAULT '0',
  `unbandate` int(10) unsigned NOT NULL DEFAULT '0',
  `bannedby` varchar(50) NOT NULL,
  `banreason` varchar(255) NOT NULL,
  `active` tinyint(3) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`,`bandate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Ban List';

-- ----------------------------
-- Records of account_banned
-- ----------------------------
INSERT INTO `account_banned` VALUES ('6', '1356282659', '1356282660', 'Xzfv', 'читак', '0');

-- ----------------------------
-- Table structure for `account_premium`
-- ----------------------------
DROP TABLE IF EXISTS `account_premium`;
CREATE TABLE `account_premium` (
  `id` int(11) NOT NULL DEFAULT '0' COMMENT 'Account id',
  `setdate` bigint(40) NOT NULL DEFAULT '0',
  `unsetdate` bigint(40) NOT NULL DEFAULT '0',
  `premium_type` tinyint(4) unsigned NOT NULL DEFAULT '1',
  `active` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`,`setdate`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='Premium Accounts';

-- ----------------------------
-- Records of account_premium
-- ----------------------------
INSERT INTO `account_premium` VALUES ('10', '0', '0', '1', '1');

-- ----------------------------
-- Table structure for `character_history`
-- ----------------------------
DROP TABLE IF EXISTS `character_history`;
CREATE TABLE `character_history` (
  `HistoryId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `AccountId` int(10) unsigned NOT NULL DEFAULT '0',
  `RealmId` int(10) unsigned NOT NULL DEFAULT '0',
  `SessionIP` varchar(32) NOT NULL,
  `HistoryType` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `CharacterGuid` int(10) unsigned NOT NULL DEFAULT '0',
  `CharacterName` varchar(12) NOT NULL,
  `HistoryTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`HistoryId`)
) ENGINE=MyISAM AUTO_INCREMENT=429 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of character_history
-- ----------------------------
INSERT INTO `character_history` VALUES ('1', '2', '1', '127.0.0.1', '1', '72', 'Мма', '2012-12-31 13:46:49');
INSERT INTO `character_history` VALUES ('2', '2', '1', '127.0.0.1', '2', '72', 'Мма', '2012-12-31 14:04:49');
INSERT INTO `character_history` VALUES ('3', '2', '1', '127.0.0.1', '1', '72', 'Мма', '2012-12-31 18:25:50');
INSERT INTO `character_history` VALUES ('4', '2', '1', '127.0.0.1', '2', '72', 'Мма', '2012-12-31 18:31:56');
INSERT INTO `character_history` VALUES ('5', '2', '1', '127.0.0.1', '1', '72', 'Мма', '2013-01-01 01:35:11');
INSERT INTO `character_history` VALUES ('6', '2', '1', '127.0.0.1', '2', '72', 'Мма', '2013-01-01 01:44:08');
INSERT INTO `character_history` VALUES ('7', '2', '1', '127.0.0.1', '1', '72', 'Мма', '2013-01-01 01:53:44');
INSERT INTO `character_history` VALUES ('8', '2', '1', '127.0.0.1', '2', '72', 'Мма', '2013-01-01 02:00:06');
INSERT INTO `character_history` VALUES ('9', '2', '1', '127.0.0.1', '1', '72', 'Мма', '2013-01-01 02:01:27');
INSERT INTO `character_history` VALUES ('10', '2', '1', '127.0.0.1', '2', '72', 'Мма', '2013-01-01 02:09:32');
INSERT INTO `character_history` VALUES ('11', '2', '1', '127.0.0.1', '1', '72', 'Мма', '2013-01-02 00:13:13');
INSERT INTO `character_history` VALUES ('12', '2', '1', '127.0.0.1', '2', '72', 'Мма', '2013-01-02 00:14:44');
INSERT INTO `character_history` VALUES ('13', '2', '1', '127.0.0.1', '1', '72', 'Мма', '2013-01-02 00:23:19');
INSERT INTO `character_history` VALUES ('14', '2', '1', '127.0.0.1', '1', '72', 'Мма', '2013-01-02 00:33:44');
INSERT INTO `character_history` VALUES ('15', '2', '1', '127.0.0.1', '2', '72', 'Мма', '2013-01-02 00:35:05');
INSERT INTO `character_history` VALUES ('16', '2', '1', '127.0.0.1', '1', '72', 'Мма', '2013-01-02 00:50:24');
INSERT INTO `character_history` VALUES ('17', '2', '1', '127.0.0.1', '2', '72', 'Мма', '2013-01-02 00:50:55');
INSERT INTO `character_history` VALUES ('18', '2', '1', '127.0.0.1', '1', '87', 'Шггш', '2013-01-02 00:51:47');
INSERT INTO `character_history` VALUES ('19', '2', '1', '127.0.0.1', '2', '87', 'Шггш', '2013-01-02 00:57:22');
INSERT INTO `character_history` VALUES ('20', '2', '1', '127.0.0.1', '1', '72', 'Мма', '2013-01-06 18:36:21');
INSERT INTO `character_history` VALUES ('21', '2', '1', '127.0.0.1', '2', '72', 'Мма', '2013-01-06 18:38:15');
INSERT INTO `character_history` VALUES ('22', '2', '1', '127.0.0.1', '1', '72', 'Мма', '2013-01-06 18:40:37');
INSERT INTO `character_history` VALUES ('23', '2', '1', '127.0.0.1', '2', '72', 'Мма', '2013-01-06 18:43:46');
INSERT INTO `character_history` VALUES ('24', '2', '1', '127.0.0.1', '1', '72', 'Мма', '2013-01-06 18:47:55');
INSERT INTO `character_history` VALUES ('25', '2', '1', '127.0.0.1', '2', '72', 'Мма', '2013-01-06 18:48:32');
INSERT INTO `character_history` VALUES ('26', '2', '1', '127.0.0.1', '1', '72', 'Мма', '2013-01-06 18:50:54');
INSERT INTO `character_history` VALUES ('27', '2', '1', '127.0.0.1', '2', '72', 'Мма', '2013-01-06 18:51:20');
INSERT INTO `character_history` VALUES ('28', '2', '1', '127.0.0.1', '1', '72', 'Мма', '2013-01-06 19:49:59');
INSERT INTO `character_history` VALUES ('29', '2', '1', '127.0.0.1', '2', '72', 'Мма', '2013-01-06 19:50:25');
INSERT INTO `character_history` VALUES ('30', '2', '1', '127.0.0.1', '1', '72', 'Мма', '2013-01-06 19:52:26');
INSERT INTO `character_history` VALUES ('31', '2', '1', '127.0.0.1', '2', '72', 'Мма', '2013-01-06 19:52:32');
INSERT INTO `character_history` VALUES ('32', '2', '1', '127.0.0.1', '1', '72', 'Мма', '2013-01-06 20:10:02');
INSERT INTO `character_history` VALUES ('33', '2', '1', '127.0.0.1', '2', '72', 'Мма', '2013-01-06 20:14:12');
INSERT INTO `character_history` VALUES ('34', '2', '1', '127.0.0.1', '1', '72', 'Мма', '2013-01-06 20:22:25');
INSERT INTO `character_history` VALUES ('35', '2', '1', '127.0.0.1', '2', '72', 'Мма', '2013-01-06 20:23:52');
INSERT INTO `character_history` VALUES ('36', '2', '1', '127.0.0.1', '1', '72', 'Мма', '2013-01-06 21:51:03');
INSERT INTO `character_history` VALUES ('37', '2', '1', '127.0.0.1', '1', '72', 'Мма', '2013-01-06 22:06:42');
INSERT INTO `character_history` VALUES ('38', '2', '1', '127.0.0.1', '1', '26', 'Hjbh', '2013-01-06 22:14:56');
INSERT INTO `character_history` VALUES ('39', '2', '1', '127.0.0.1', '1', '26', 'Hjbh', '2013-01-06 22:23:46');
INSERT INTO `character_history` VALUES ('40', '2', '1', '127.0.0.1', '1', '72', 'Мма', '2013-01-06 22:49:57');
INSERT INTO `character_history` VALUES ('41', '2', '1', '127.0.0.1', '1', '72', 'Мма', '2013-01-06 22:57:33');
INSERT INTO `character_history` VALUES ('42', '2', '1', '127.0.0.1', '1', '31', 'Xzpr', '2013-01-12 15:59:22');
INSERT INTO `character_history` VALUES ('43', '2', '1', '127.0.0.1', '2', '31', 'Xzpr', '2013-01-12 15:59:51');
INSERT INTO `character_history` VALUES ('44', '2', '1', '127.0.0.1', '1', '31', 'Xzpr', '2013-01-12 17:16:39');
INSERT INTO `character_history` VALUES ('45', '2', '1', '127.0.0.1', '2', '31', 'Xzpr', '2013-01-12 17:19:59');
INSERT INTO `character_history` VALUES ('46', '2', '1', '127.0.0.1', '1', '31', 'Xzpr', '2013-01-12 17:20:07');
INSERT INTO `character_history` VALUES ('47', '2', '1', '127.0.0.1', '2', '31', 'Xzpr', '2013-01-12 17:20:19');
INSERT INTO `character_history` VALUES ('48', '2', '1', '127.0.0.1', '1', '31', 'Xzpr', '2013-01-12 17:25:09');
INSERT INTO `character_history` VALUES ('49', '2', '1', '127.0.0.1', '2', '31', 'Xzpr', '2013-01-12 17:27:49');
INSERT INTO `character_history` VALUES ('50', '2', '1', '127.0.0.1', '1', '31', 'Xzpr', '2013-01-12 18:00:21');
INSERT INTO `character_history` VALUES ('51', '2', '1', '127.0.0.1', '2', '31', 'Xzpr', '2013-01-12 18:00:45');
INSERT INTO `character_history` VALUES ('52', '1', '1', '127.0.0.1', '1', '30', 'Crashik', '2013-01-13 00:46:31');
INSERT INTO `character_history` VALUES ('53', '2', '1', '127.0.0.1', '1', '31', 'Xzpr', '2013-01-13 02:43:42');
INSERT INTO `character_history` VALUES ('54', '2', '1', '127.0.0.1', '3', '49', 'Уркен', '2013-01-13 03:11:57');
INSERT INTO `character_history` VALUES ('55', '2', '1', '127.0.0.1', '3', '26', 'Hjbh', '2013-01-13 03:12:06');
INSERT INTO `character_history` VALUES ('56', '2', '1', '127.0.0.1', '3', '45', 'Testik', '2013-01-13 03:12:10');
INSERT INTO `character_history` VALUES ('57', '2', '1', '127.0.0.1', '3', '53', 'Пукп', '2013-01-13 03:12:15');
INSERT INTO `character_history` VALUES ('58', '2', '1', '127.0.0.1', '3', '76', 'Уаау', '2013-01-13 03:12:23');
INSERT INTO `character_history` VALUES ('59', '2', '1', '127.0.0.1', '3', '31', 'Xzpr', '2013-01-13 03:12:32');
INSERT INTO `character_history` VALUES ('60', '2', '1', '127.0.0.1', '3', '83', 'Вуау', '2013-01-13 03:12:36');
INSERT INTO `character_history` VALUES ('61', '2', '1', '127.0.0.1', '3', '84', 'Fregrgtr', '2013-01-13 03:12:42');
INSERT INTO `character_history` VALUES ('62', '2', '1', '127.0.0.1', '3', '87', 'Шггш', '2013-01-13 03:12:46');
INSERT INTO `character_history` VALUES ('63', '2', '1', '127.0.0.1', '1', '72', 'Мма', '2013-01-13 03:13:13');
INSERT INTO `character_history` VALUES ('64', '2', '1', '127.0.0.1', '1', '72', 'Мма', '2013-01-13 03:29:51');
INSERT INTO `character_history` VALUES ('65', '2', '1', '127.0.0.1', '1', '72', 'Мма', '2013-01-13 03:47:54');
INSERT INTO `character_history` VALUES ('66', '2', '1', '127.0.0.1', '2', '72', 'Мма', '2013-01-13 03:49:47');
INSERT INTO `character_history` VALUES ('67', '2', '1', '127.0.0.1', '1', '72', 'Мма', '2013-01-13 22:46:00');
INSERT INTO `character_history` VALUES ('68', '2', '1', '127.0.0.1', '2', '72', 'Мма', '2013-01-13 22:55:07');
INSERT INTO `character_history` VALUES ('69', '2', '1', '127.0.0.1', '1', '72', 'Мма', '2013-01-14 00:07:50');
INSERT INTO `character_history` VALUES ('70', '3', '1', '127.0.0.1', '1', '67', 'Лёшара', '2013-01-14 00:20:24');
INSERT INTO `character_history` VALUES ('71', '3', '1', '127.0.0.1', '2', '67', 'Лёшара', '2013-01-14 00:24:05');
INSERT INTO `character_history` VALUES ('72', '2', '1', '127.0.0.1', '2', '72', 'Мма', '2013-01-14 00:28:00');
INSERT INTO `character_history` VALUES ('73', '2', '1', '127.0.0.1', '1', '72', 'Мма', '2013-01-15 19:28:58');
INSERT INTO `character_history` VALUES ('74', '2', '1', '127.0.0.1', '2', '72', 'Мма', '2013-01-15 19:29:59');
INSERT INTO `character_history` VALUES ('75', '2', '1', '127.0.0.1', '1', '72', 'Мма', '2013-01-15 19:30:09');
INSERT INTO `character_history` VALUES ('76', '2', '1', '127.0.0.1', '2', '72', 'Мма', '2013-01-15 19:39:25');
INSERT INTO `character_history` VALUES ('77', '2', '1', '127.0.0.1', '1', '72', 'Мма', '2013-01-16 20:58:34');
INSERT INTO `character_history` VALUES ('78', '2', '1', '127.0.0.1', '2', '72', 'Мма', '2013-01-16 21:00:18');
INSERT INTO `character_history` VALUES ('79', '2', '1', '127.0.0.1', '1', '72', 'Мма', '2013-01-18 01:04:59');
INSERT INTO `character_history` VALUES ('80', '2', '1', '127.0.0.1', '2', '72', 'Мма', '2013-01-18 01:08:07');
INSERT INTO `character_history` VALUES ('81', '2', '1', '127.0.0.1', '1', '72', 'Мма', '2013-01-19 12:00:54');
INSERT INTO `character_history` VALUES ('82', '3', '1', '127.0.0.1', '4', '89', 'Арп', '2013-01-19 12:01:48');
INSERT INTO `character_history` VALUES ('83', '3', '1', '127.0.0.1', '1', '89', 'Арп', '2013-01-19 12:02:14');
INSERT INTO `character_history` VALUES ('84', '3', '1', '127.0.0.1', '2', '89', 'Арп', '2013-01-19 12:04:00');
INSERT INTO `character_history` VALUES ('85', '3', '1', '127.0.0.1', '1', '89', 'Арп', '2013-01-19 12:05:37');
INSERT INTO `character_history` VALUES ('86', '2', '1', '127.0.0.1', '2', '72', 'Мма', '2013-01-19 12:06:28');
INSERT INTO `character_history` VALUES ('87', '3', '1', '127.0.0.1', '2', '89', 'Арп', '2013-01-19 12:06:30');
INSERT INTO `character_history` VALUES ('88', '2', '1', '127.0.0.1', '1', '72', 'Мма', '2013-01-20 17:22:31');
INSERT INTO `character_history` VALUES ('89', '2', '1', '127.0.0.1', '2', '72', 'Мма', '2013-01-20 17:40:53');
INSERT INTO `character_history` VALUES ('90', '2', '1', '127.0.0.1', '1', '72', 'Мма', '2013-01-20 20:04:12');
INSERT INTO `character_history` VALUES ('91', '2', '1', '127.0.0.1', '2', '72', 'Мма', '2013-01-20 20:22:55');
INSERT INTO `character_history` VALUES ('92', '2', '1', '127.0.0.1', '1', '72', 'Мма', '2013-01-20 21:19:59');
INSERT INTO `character_history` VALUES ('93', '2', '1', '127.0.0.1', '2', '72', 'Мма', '2013-01-20 21:24:31');
INSERT INTO `character_history` VALUES ('94', '2', '1', '127.0.0.1', '1', '72', 'Мма', '2013-01-25 16:18:28');
INSERT INTO `character_history` VALUES ('95', '2', '1', '127.0.0.1', '2', '72', 'Мма', '2013-01-25 16:40:01');
INSERT INTO `character_history` VALUES ('96', '2', '1', '127.0.0.1', '1', '72', 'Мма', '2013-01-25 16:41:51');
INSERT INTO `character_history` VALUES ('97', '2', '1', '127.0.0.1', '2', '72', 'Мма', '2013-01-25 16:46:40');
INSERT INTO `character_history` VALUES ('98', '2', '1', '127.0.0.1', '1', '72', 'Мма', '2013-01-25 17:31:57');
INSERT INTO `character_history` VALUES ('99', '2', '1', '127.0.0.1', '2', '72', 'Мма', '2013-01-25 17:36:26');
INSERT INTO `character_history` VALUES ('100', '2', '1', '127.0.0.1', '4', '90', 'Ggrg', '2013-01-25 17:36:42');
INSERT INTO `character_history` VALUES ('101', '2', '1', '127.0.0.1', '1', '90', 'Ggrg', '2013-01-25 17:36:52');
INSERT INTO `character_history` VALUES ('102', '2', '1', '127.0.0.1', '2', '90', 'Ggrg', '2013-01-25 17:41:03');
INSERT INTO `character_history` VALUES ('103', '2', '1', '127.0.0.1', '1', '72', 'Мма', '2013-01-25 22:31:20');
INSERT INTO `character_history` VALUES ('104', '2', '1', '127.0.0.1', '2', '72', 'Мма', '2013-01-25 22:31:49');
INSERT INTO `character_history` VALUES ('105', '2', '1', '127.0.0.1', '1', '72', 'Мма', '2013-01-25 22:31:59');
INSERT INTO `character_history` VALUES ('106', '2', '1', '127.0.0.1', '2', '72', 'Мма', '2013-01-25 22:34:22');
INSERT INTO `character_history` VALUES ('107', '2', '1', '127.0.0.1', '1', '90', 'Ggrg', '2013-01-25 22:34:32');
INSERT INTO `character_history` VALUES ('108', '2', '1', '127.0.0.1', '2', '90', 'Ggrg', '2013-01-25 22:47:31');
INSERT INTO `character_history` VALUES ('109', '2', '1', '127.0.0.1', '1', '72', 'Мма', '2013-01-26 20:10:48');
INSERT INTO `character_history` VALUES ('110', '2', '1', '127.0.0.1', '2', '72', 'Мма', '2013-01-26 20:24:49');
INSERT INTO `character_history` VALUES ('111', '2', '1', '127.0.0.1', '1', '72', 'Мма', '2013-01-26 21:25:39');
INSERT INTO `character_history` VALUES ('112', '2', '1', '127.0.0.1', '2', '72', 'Мма', '2013-01-26 21:27:47');
INSERT INTO `character_history` VALUES ('113', '2', '1', '127.0.0.1', '4', '91', 'Ulduar', '2013-01-26 21:28:01');
INSERT INTO `character_history` VALUES ('114', '2', '1', '127.0.0.1', '1', '91', 'Ulduar', '2013-01-26 21:28:13');
INSERT INTO `character_history` VALUES ('115', '2', '1', '127.0.0.1', '2', '91', 'Ulduar', '2013-01-26 21:34:59');
INSERT INTO `character_history` VALUES ('116', '2', '1', '127.0.0.1', '1', '72', 'Мма', '2013-02-02 23:53:26');
INSERT INTO `character_history` VALUES ('117', '2', '1', '127.0.0.1', '2', '72', 'Мма', '2013-02-02 23:53:43');
INSERT INTO `character_history` VALUES ('118', '2', '1', '127.0.0.1', '1', '72', 'Мма', '2013-02-03 00:04:29');
INSERT INTO `character_history` VALUES ('119', '2', '1', '127.0.0.1', '2', '72', 'Мма', '2013-02-03 00:05:39');
INSERT INTO `character_history` VALUES ('120', '2', '1', '127.0.0.1', '1', '72', 'Мма', '2013-02-03 00:05:51');
INSERT INTO `character_history` VALUES ('121', '12', '1', '109.254.70.192', '4', '92', 'Dereviashaka', '2013-02-03 00:05:54');
INSERT INTO `character_history` VALUES ('122', '2', '1', '127.0.0.1', '2', '72', 'Мма', '2013-02-03 00:06:04');
INSERT INTO `character_history` VALUES ('123', '12', '1', '109.254.70.192', '1', '92', 'Dereviashaka', '2013-02-03 00:06:35');
INSERT INTO `character_history` VALUES ('124', '2', '1', '127.0.0.1', '1', '72', 'Мма', '2013-02-03 00:08:14');
INSERT INTO `character_history` VALUES ('125', '2', '1', '127.0.0.1', '2', '72', 'Мма', '2013-02-03 00:14:26');
INSERT INTO `character_history` VALUES ('126', '2', '1', '127.0.0.1', '4', '93', 'Mmoo', '2013-02-03 00:14:40');
INSERT INTO `character_history` VALUES ('127', '2', '1', '127.0.0.1', '1', '93', 'Mmoo', '2013-02-03 00:14:49');
INSERT INTO `character_history` VALUES ('128', '12', '1', '109.254.70.192', '2', '92', 'Dereviashaka', '2013-02-03 00:16:56');
INSERT INTO `character_history` VALUES ('129', '12', '1', '109.254.70.192', '1', '92', 'Dereviashaka', '2013-02-03 00:17:30');
INSERT INTO `character_history` VALUES ('130', '12', '1', '109.254.70.192', '2', '92', 'Dereviashaka', '2013-02-03 00:44:43');
INSERT INTO `character_history` VALUES ('131', '12', '1', '109.254.70.192', '1', '92', 'Dereviashaka', '2013-02-03 00:45:05');
INSERT INTO `character_history` VALUES ('132', '12', '1', '109.254.70.192', '2', '92', 'Dereviashaka', '2013-02-03 00:50:01');
INSERT INTO `character_history` VALUES ('133', '12', '1', '109.254.70.192', '1', '92', 'Dereviashaka', '2013-02-03 00:50:13');
INSERT INTO `character_history` VALUES ('134', '2', '1', '127.0.0.1', '2', '93', 'Mmoo', '2013-02-03 01:05:20');
INSERT INTO `character_history` VALUES ('135', '12', '1', '109.254.70.192', '2', '92', 'Dereviashaka', '2013-02-03 01:05:52');
INSERT INTO `character_history` VALUES ('136', '2', '1', '127.0.0.1', '1', '93', 'Mmoo', '2013-02-03 20:34:48');
INSERT INTO `character_history` VALUES ('137', '2', '1', '127.0.0.1', '2', '93', 'Mmoo', '2013-02-03 20:37:32');
INSERT INTO `character_history` VALUES ('138', '2', '1', '127.0.0.1', '4', '94', 'Crashaman', '2013-02-04 21:32:06');
INSERT INTO `character_history` VALUES ('139', '2', '1', '127.0.0.1', '1', '94', 'Crashaman', '2013-02-04 21:32:29');
INSERT INTO `character_history` VALUES ('140', '2', '1', '127.0.0.1', '2', '94', 'Crashaman', '2013-02-04 21:35:33');
INSERT INTO `character_history` VALUES ('141', '2', '1', '127.0.0.1', '1', '93', 'Mmoo', '2013-02-05 21:01:35');
INSERT INTO `character_history` VALUES ('142', '11', '1', '31.129.121.1', '4', '95', 'Tiraelife', '2013-02-05 21:03:49');
INSERT INTO `character_history` VALUES ('143', '11', '1', '31.129.121.1', '1', '95', 'Tiraelife', '2013-02-05 21:05:33');
INSERT INTO `character_history` VALUES ('144', '12', '1', '109.254.70.192', '1', '92', 'Dereviashaka', '2013-02-05 21:08:07');
INSERT INTO `character_history` VALUES ('145', '12', '1', '109.254.70.192', '2', '92', 'Dereviashaka', '2013-02-05 21:30:35');
INSERT INTO `character_history` VALUES ('146', '12', '1', '109.254.70.192', '1', '92', 'Dereviashaka', '2013-02-05 21:31:15');
INSERT INTO `character_history` VALUES ('147', '11', '1', '31.129.121.1', '2', '95', 'Tiraelife', '2013-02-05 21:53:25');
INSERT INTO `character_history` VALUES ('148', '2', '1', '127.0.0.1', '2', '93', 'Mmoo', '2013-02-05 22:21:13');
INSERT INTO `character_history` VALUES ('149', '2', '1', '127.0.0.1', '1', '93', 'Mmoo', '2013-02-05 22:21:47');
INSERT INTO `character_history` VALUES ('150', '12', '1', '109.254.70.192', '2', '92', 'Dereviashaka', '2013-02-05 22:22:24');
INSERT INTO `character_history` VALUES ('151', '12', '1', '109.254.70.192', '1', '92', 'Dereviashaka', '2013-02-05 22:24:02');
INSERT INTO `character_history` VALUES ('152', '2', '1', '127.0.0.1', '2', '93', 'Mmoo', '2013-02-05 22:27:17');
INSERT INTO `character_history` VALUES ('153', '12', '1', '109.254.70.192', '2', '92', 'Dereviashaka', '2013-02-05 22:27:39');
INSERT INTO `character_history` VALUES ('154', '2', '1', '127.0.0.1', '1', '93', 'Mmoo', '2013-02-05 22:27:41');
INSERT INTO `character_history` VALUES ('155', '12', '1', '109.254.70.192', '1', '92', 'Dereviashaka', '2013-02-05 22:28:05');
INSERT INTO `character_history` VALUES ('156', '12', '1', '109.254.70.192', '2', '92', 'Dereviashaka', '2013-02-05 22:33:45');
INSERT INTO `character_history` VALUES ('157', '12', '1', '109.254.70.192', '1', '92', 'Dereviashaka', '2013-02-05 22:34:11');
INSERT INTO `character_history` VALUES ('158', '11', '1', '31.129.123.15', '4', '96', 'Архангел', '2013-02-05 22:46:58');
INSERT INTO `character_history` VALUES ('159', '11', '1', '31.129.123.15', '1', '96', 'Архангел', '2013-02-05 22:47:20');
INSERT INTO `character_history` VALUES ('160', '2', '1', '127.0.0.1', '2', '93', 'Mmoo', '2013-02-05 22:49:41');
INSERT INTO `character_history` VALUES ('161', '2', '1', '127.0.0.1', '1', '93', 'Mmoo', '2013-02-05 22:50:05');
INSERT INTO `character_history` VALUES ('162', '12', '1', '109.254.70.192', '2', '92', 'Dereviashaka', '2013-02-05 22:50:05');
INSERT INTO `character_history` VALUES ('163', '12', '1', '109.254.70.192', '1', '92', 'Dereviashaka', '2013-02-05 22:51:47');
INSERT INTO `character_history` VALUES ('164', '2', '1', '127.0.0.1', '2', '93', 'Mmoo', '2013-02-05 23:03:37');
INSERT INTO `character_history` VALUES ('165', '2', '1', '127.0.0.1', '1', '93', 'Mmoo', '2013-02-05 23:04:03');
INSERT INTO `character_history` VALUES ('166', '12', '1', '109.254.70.192', '2', '92', 'Dereviashaka', '2013-02-05 23:22:00');
INSERT INTO `character_history` VALUES ('167', '11', '1', '31.129.123.15', '2', '96', 'Архангел', '2013-02-05 23:31:56');
INSERT INTO `character_history` VALUES ('168', '2', '1', '127.0.0.1', '2', '93', 'Mmoo', '2013-02-05 23:32:23');
INSERT INTO `character_history` VALUES ('169', '2', '1', '127.0.0.1', '1', '93', 'Mmoo', '2013-02-06 14:52:27');
INSERT INTO `character_history` VALUES ('170', '12', '1', '109.254.70.192', '1', '92', 'Dereviashaka', '2013-02-06 15:00:33');
INSERT INTO `character_history` VALUES ('171', '12', '1', '109.254.70.192', '2', '92', 'Dereviashaka', '2013-02-06 15:19:30');
INSERT INTO `character_history` VALUES ('172', '12', '1', '109.254.70.192', '1', '92', 'Dereviashaka', '2013-02-06 15:19:48');
INSERT INTO `character_history` VALUES ('173', '11', '1', '31.129.121.78', '1', '96', 'Архангел', '2013-02-06 15:24:51');
INSERT INTO `character_history` VALUES ('174', '11', '1', '31.129.121.78', '2', '96', 'Архангел', '2013-02-06 15:32:50');
INSERT INTO `character_history` VALUES ('175', '11', '1', '31.129.123.137', '1', '96', 'Архангел', '2013-02-06 15:36:48');
INSERT INTO `character_history` VALUES ('176', '12', '1', '109.254.70.192', '2', '92', 'Dereviashaka', '2013-02-06 15:38:19');
INSERT INTO `character_history` VALUES ('177', '11', '1', '31.129.123.137', '2', '96', 'Архангел', '2013-02-06 15:38:24');
INSERT INTO `character_history` VALUES ('178', '12', '1', '109.254.70.192', '1', '92', 'Dereviashaka', '2013-02-06 15:38:48');
INSERT INTO `character_history` VALUES ('179', '11', '1', '31.129.123.137', '1', '96', 'Архангел', '2013-02-06 15:39:59');
INSERT INTO `character_history` VALUES ('180', '12', '1', '109.254.70.192', '1', '92', 'Dereviashaka', '2013-02-06 15:55:43');
INSERT INTO `character_history` VALUES ('181', '2', '1', '127.0.0.1', '1', '93', 'Mmoo', '2013-02-06 15:57:44');
INSERT INTO `character_history` VALUES ('182', '11', '1', '31.129.123.137', '1', '96', 'Архангел', '2013-02-06 15:57:49');
INSERT INTO `character_history` VALUES ('183', '11', '1', '31.129.123.137', '2', '96', 'Архангел', '2013-02-06 16:40:18');
INSERT INTO `character_history` VALUES ('184', '11', '1', '31.129.123.137', '1', '96', 'Архангел', '2013-02-06 16:41:34');
INSERT INTO `character_history` VALUES ('185', '2', '1', '127.0.0.1', '2', '93', 'Mmoo', '2013-02-06 16:42:55');
INSERT INTO `character_history` VALUES ('186', '2', '1', '127.0.0.1', '1', '93', 'Mmoo', '2013-02-06 16:43:23');
INSERT INTO `character_history` VALUES ('187', '2', '1', '127.0.0.1', '2', '93', 'Mmoo', '2013-02-06 17:16:17');
INSERT INTO `character_history` VALUES ('188', '11', '1', '31.129.123.137', '2', '96', 'Архангел', '2013-02-06 18:00:21');
INSERT INTO `character_history` VALUES ('189', '11', '1', '31.129.123.137', '1', '96', 'Архангел', '2013-02-06 18:01:43');
INSERT INTO `character_history` VALUES ('190', '11', '1', '31.129.123.137', '2', '96', 'Архангел', '2013-02-06 18:40:17');
INSERT INTO `character_history` VALUES ('191', '12', '1', '109.254.70.192', '2', '92', 'Dereviashaka', '2013-02-06 18:40:26');
INSERT INTO `character_history` VALUES ('192', '12', '1', '109.254.70.192', '1', '92', 'Dereviashaka', '2013-02-06 18:41:23');
INSERT INTO `character_history` VALUES ('193', '11', '1', '31.129.123.137', '1', '96', 'Архангел', '2013-02-06 18:43:02');
INSERT INTO `character_history` VALUES ('194', '11', '1', '31.129.123.137', '2', '96', 'Архангел', '2013-02-06 18:57:21');
INSERT INTO `character_history` VALUES ('195', '11', '1', '31.129.123.137', '1', '96', 'Архангел', '2013-02-06 18:58:23');
INSERT INTO `character_history` VALUES ('196', '11', '1', '31.129.123.137', '2', '96', 'Архангел', '2013-02-06 19:03:49');
INSERT INTO `character_history` VALUES ('197', '12', '1', '109.254.70.192', '2', '92', 'Dereviashaka', '2013-02-06 19:33:07');
INSERT INTO `character_history` VALUES ('198', '2', '1', '127.0.0.1', '1', '93', 'Mmoo', '2013-02-06 20:58:55');
INSERT INTO `character_history` VALUES ('199', '6', '1', '178.127.6.58', '1', '32', 'Hero', '2013-02-06 21:02:40');
INSERT INTO `character_history` VALUES ('200', '6', '1', '178.127.6.58', '2', '32', 'Hero', '2013-02-06 21:03:12');
INSERT INTO `character_history` VALUES ('201', '6', '1', '178.127.6.58', '1', '32', 'Hero', '2013-02-06 21:03:35');
INSERT INTO `character_history` VALUES ('202', '6', '1', '178.127.6.58', '2', '32', 'Hero', '2013-02-06 21:06:49');
INSERT INTO `character_history` VALUES ('203', '2', '1', '127.0.0.1', '1', '93', 'Mmoo', '2013-02-06 21:11:29');
INSERT INTO `character_history` VALUES ('204', '2', '1', '127.0.0.1', '2', '93', 'Mmoo', '2013-02-06 21:11:45');
INSERT INTO `character_history` VALUES ('205', '2', '1', '127.0.0.1', '1', '93', 'Mmoo', '2013-02-07 16:01:13');
INSERT INTO `character_history` VALUES ('206', '2', '1', '127.0.0.1', '2', '93', 'Mmoo', '2013-02-07 16:04:17');
INSERT INTO `character_history` VALUES ('207', '2', '1', '127.0.0.1', '1', '93', 'Mmoo', '2013-02-07 16:19:16');
INSERT INTO `character_history` VALUES ('208', '2', '1', '127.0.0.1', '2', '93', 'Mmoo', '2013-02-07 16:19:35');
INSERT INTO `character_history` VALUES ('209', '2', '1', '127.0.0.1', '1', '93', 'Mmoo', '2013-02-07 19:51:54');
INSERT INTO `character_history` VALUES ('210', '2', '1', '127.0.0.1', '2', '93', 'Mmoo', '2013-02-07 20:26:14');
INSERT INTO `character_history` VALUES ('211', '2', '1', '127.0.0.1', '1', '93', 'Mmoo', '2013-02-07 20:26:53');
INSERT INTO `character_history` VALUES ('212', '2', '1', '127.0.0.1', '2', '93', 'Mmoo', '2013-02-07 20:29:31');
INSERT INTO `character_history` VALUES ('213', '2', '1', '127.0.0.1', '1', '93', 'Mmoo', '2013-02-07 20:37:18');
INSERT INTO `character_history` VALUES ('214', '2', '1', '127.0.0.1', '2', '93', 'Mmoo', '2013-02-07 20:41:08');
INSERT INTO `character_history` VALUES ('215', '2', '1', '127.0.0.1', '1', '93', 'Mmoo', '2013-02-07 20:43:43');
INSERT INTO `character_history` VALUES ('216', '2', '1', '127.0.0.1', '2', '93', 'Mmoo', '2013-02-07 20:48:11');
INSERT INTO `character_history` VALUES ('217', '2', '1', '127.0.0.1', '1', '93', 'Mmoo', '2013-02-07 21:59:26');
INSERT INTO `character_history` VALUES ('218', '2', '1', '127.0.0.1', '2', '93', 'Mmoo', '2013-02-07 22:12:03');
INSERT INTO `character_history` VALUES ('219', '2', '1', '127.0.0.1', '1', '93', 'Mmoo', '2013-02-07 22:18:04');
INSERT INTO `character_history` VALUES ('220', '2', '1', '127.0.0.1', '2', '93', 'Mmoo', '2013-02-07 22:20:52');
INSERT INTO `character_history` VALUES ('221', '2', '1', '127.0.0.1', '1', '93', 'Mmoo', '2013-02-07 22:21:01');
INSERT INTO `character_history` VALUES ('222', '2', '1', '127.0.0.1', '2', '93', 'Mmoo', '2013-02-07 22:47:29');
INSERT INTO `character_history` VALUES ('223', '2', '1', '127.0.0.1', '1', '93', 'Mmoo', '2013-02-07 22:50:04');
INSERT INTO `character_history` VALUES ('224', '2', '1', '127.0.0.1', '2', '93', 'Mmoo', '2013-02-07 23:04:31');
INSERT INTO `character_history` VALUES ('225', '2', '1', '127.0.0.1', '1', '93', 'Mmoo', '2013-02-07 23:08:59');
INSERT INTO `character_history` VALUES ('226', '2', '1', '127.0.0.1', '1', '93', 'Mmoo', '2013-02-07 23:19:29');
INSERT INTO `character_history` VALUES ('227', '2', '1', '127.0.0.1', '2', '93', 'Mmoo', '2013-02-07 23:54:03');
INSERT INTO `character_history` VALUES ('228', '2', '1', '127.0.0.1', '1', '93', 'Mmoo', '2013-02-09 11:06:07');
INSERT INTO `character_history` VALUES ('229', '2', '1', '127.0.0.1', '2', '93', 'Mmoo', '2013-02-09 11:20:14');
INSERT INTO `character_history` VALUES ('230', '2', '1', '127.0.0.1', '1', '93', 'Mmoo', '2013-02-10 21:51:17');
INSERT INTO `character_history` VALUES ('231', '2', '1', '127.0.0.1', '2', '93', 'Mmoo', '2013-02-10 22:17:03');
INSERT INTO `character_history` VALUES ('232', '2', '1', '127.0.0.1', '1', '93', 'Mmoo', '2013-02-10 22:18:04');
INSERT INTO `character_history` VALUES ('233', '2', '1', '127.0.0.1', '2', '93', 'Mmoo', '2013-02-10 22:18:40');
INSERT INTO `character_history` VALUES ('234', '2', '1', '127.0.0.1', '1', '93', 'Mmoo', '2013-02-10 22:25:18');
INSERT INTO `character_history` VALUES ('235', '2', '1', '127.0.0.1', '2', '93', 'Mmoo', '2013-02-10 22:26:19');
INSERT INTO `character_history` VALUES ('236', '2', '1', '127.0.0.1', '1', '93', 'Mmoo', '2013-02-10 22:28:13');
INSERT INTO `character_history` VALUES ('237', '2', '1', '127.0.0.1', '2', '93', 'Mmoo', '2013-02-10 22:28:29');
INSERT INTO `character_history` VALUES ('238', '2', '1', '127.0.0.1', '1', '93', 'Mmoo', '2013-02-10 22:33:56');
INSERT INTO `character_history` VALUES ('239', '2', '1', '127.0.0.1', '2', '93', 'Mmoo', '2013-02-10 23:04:55');
INSERT INTO `character_history` VALUES ('240', '2', '1', '127.0.0.1', '1', '93', 'Mmoo', '2013-02-10 23:16:43');
INSERT INTO `character_history` VALUES ('241', '2', '1', '127.0.0.1', '2', '93', 'Mmoo', '2013-02-10 23:31:06');
INSERT INTO `character_history` VALUES ('242', '2', '1', '127.0.0.1', '1', '93', 'Mmoo', '2013-02-10 23:46:17');
INSERT INTO `character_history` VALUES ('243', '2', '1', '127.0.0.1', '2', '93', 'Mmoo', '2013-02-10 23:55:38');
INSERT INTO `character_history` VALUES ('244', '2', '1', '127.0.0.1', '1', '93', 'Mmoo', '2013-02-11 23:47:07');
INSERT INTO `character_history` VALUES ('245', '3', '1', '127.0.0.1', '1', '65', 'Дикообраз', '2013-02-11 23:58:24');
INSERT INTO `character_history` VALUES ('246', '3', '1', '127.0.0.1', '1', '65', 'Дикообраз', '2013-02-12 00:01:46');
INSERT INTO `character_history` VALUES ('247', '2', '1', '127.0.0.1', '1', '93', 'Mmoo', '2013-02-12 00:02:01');
INSERT INTO `character_history` VALUES ('248', '2', '1', '127.0.0.1', '2', '93', 'Mmoo', '2013-02-12 00:06:50');
INSERT INTO `character_history` VALUES ('249', '3', '1', '127.0.0.1', '2', '65', 'Дикообраз', '2013-02-12 00:06:54');
INSERT INTO `character_history` VALUES ('250', '2', '1', '127.0.0.1', '1', '93', 'Mmoo', '2013-02-12 16:14:34');
INSERT INTO `character_history` VALUES ('251', '2', '1', '127.0.0.1', '2', '93', 'Mmoo', '2013-02-12 16:16:57');
INSERT INTO `character_history` VALUES ('252', '2', '1', '127.0.0.1', '1', '93', 'Mmoo', '2013-02-12 16:30:38');
INSERT INTO `character_history` VALUES ('253', '2', '1', '127.0.0.1', '2', '93', 'Mmoo', '2013-02-12 16:31:22');
INSERT INTO `character_history` VALUES ('254', '2', '1', '127.0.0.1', '1', '93', 'Mmoo', '2013-02-12 16:33:28');
INSERT INTO `character_history` VALUES ('255', '2', '1', '127.0.0.1', '2', '93', 'Mmoo', '2013-02-12 16:34:52');
INSERT INTO `character_history` VALUES ('256', '2', '1', '127.0.0.1', '1', '93', 'Mmoo', '2013-02-12 16:41:20');
INSERT INTO `character_history` VALUES ('257', '2', '1', '127.0.0.1', '2', '93', 'Mmoo', '2013-02-12 16:42:00');
INSERT INTO `character_history` VALUES ('258', '2', '1', '127.0.0.1', '1', '93', 'Mmoo', '2013-02-12 18:32:32');
INSERT INTO `character_history` VALUES ('259', '2', '1', '127.0.0.1', '2', '93', 'Mmoo', '2013-02-12 18:45:07');
INSERT INTO `character_history` VALUES ('260', '2', '1', '127.0.0.1', '1', '93', 'Mmoo', '2013-02-13 15:41:38');
INSERT INTO `character_history` VALUES ('261', '2', '1', '127.0.0.1', '2', '93', 'Mmoo', '2013-02-13 16:03:39');
INSERT INTO `character_history` VALUES ('262', '2', '1', '127.0.0.1', '4', '97', 'Gtrh', '2013-02-13 20:30:12');
INSERT INTO `character_history` VALUES ('263', '2', '1', '127.0.0.1', '4', '98', 'Rgeg', '2013-02-13 20:30:38');
INSERT INTO `character_history` VALUES ('264', '2', '1', '127.0.0.1', '1', '98', 'Rgeg', '2013-02-13 20:31:02');
INSERT INTO `character_history` VALUES ('265', '2', '1', '127.0.0.1', '2', '98', 'Rgeg', '2013-02-13 20:38:59');
INSERT INTO `character_history` VALUES ('266', '2', '1', '127.0.0.1', '1', '93', 'Mmoo', '2013-02-13 20:39:16');
INSERT INTO `character_history` VALUES ('267', '2', '1', '127.0.0.1', '2', '93', 'Mmoo', '2013-02-13 20:45:49');
INSERT INTO `character_history` VALUES ('268', '2', '1', '127.0.0.1', '1', '98', 'Rgeg', '2013-02-14 00:50:05');
INSERT INTO `character_history` VALUES ('269', '2', '1', '127.0.0.1', '2', '98', 'Rgeg', '2013-02-14 00:50:43');
INSERT INTO `character_history` VALUES ('270', '2', '1', '127.0.0.1', '1', '98', 'Rgeg', '2013-02-14 00:51:32');
INSERT INTO `character_history` VALUES ('271', '2', '1', '127.0.0.1', '2', '98', 'Rgeg', '2013-02-14 00:53:42');
INSERT INTO `character_history` VALUES ('272', '2', '1', '127.0.0.1', '1', '98', 'Rgeg', '2013-02-14 00:57:38');
INSERT INTO `character_history` VALUES ('273', '2', '1', '127.0.0.1', '2', '98', 'Rgeg', '2013-02-14 00:58:01');
INSERT INTO `character_history` VALUES ('274', '2', '1', '127.0.0.1', '1', '98', 'Rgeg', '2013-02-14 22:25:54');
INSERT INTO `character_history` VALUES ('275', '2', '1', '127.0.0.1', '2', '98', 'Rgeg', '2013-02-14 22:32:40');
INSERT INTO `character_history` VALUES ('276', '2', '1', '127.0.0.1', '1', '98', 'Rgeg', '2013-02-15 19:10:22');
INSERT INTO `character_history` VALUES ('277', '2', '1', '127.0.0.1', '2', '98', 'Rgeg', '2013-02-15 19:14:30');
INSERT INTO `character_history` VALUES ('278', '2', '1', '127.0.0.1', '1', '98', 'Rgeg', '2013-02-15 19:16:14');
INSERT INTO `character_history` VALUES ('279', '2', '1', '127.0.0.1', '2', '98', 'Rgeg', '2013-02-15 19:17:39');
INSERT INTO `character_history` VALUES ('280', '2', '1', '127.0.0.1', '1', '98', 'Rgeg', '2013-02-15 21:57:50');
INSERT INTO `character_history` VALUES ('281', '2', '1', '127.0.0.1', '2', '98', 'Rgeg', '2013-02-15 21:58:03');
INSERT INTO `character_history` VALUES ('282', '2', '1', '127.0.0.1', '4', '99', 'Uuhyk', '2013-02-15 21:58:28');
INSERT INTO `character_history` VALUES ('283', '2', '1', '127.0.0.1', '1', '99', 'Uuhyk', '2013-02-15 21:58:41');
INSERT INTO `character_history` VALUES ('284', '2', '1', '127.0.0.1', '2', '99', 'Uuhyk', '2013-02-15 22:00:47');
INSERT INTO `character_history` VALUES ('285', '2', '1', '127.0.0.1', '1', '99', 'Uuhyk', '2013-02-15 22:12:54');
INSERT INTO `character_history` VALUES ('286', '2', '1', '127.0.0.1', '2', '99', 'Uuhyk', '2013-02-15 22:13:30');
INSERT INTO `character_history` VALUES ('287', '2', '1', '127.0.0.1', '1', '98', 'Rgeg', '2013-02-16 00:28:59');
INSERT INTO `character_history` VALUES ('288', '2', '1', '127.0.0.1', '2', '98', 'Rgeg', '2013-02-16 00:34:36');
INSERT INTO `character_history` VALUES ('289', '2', '1', '127.0.0.1', '1', '99', 'Uuhyk', '2013-02-22 17:38:20');
INSERT INTO `character_history` VALUES ('290', '2', '1', '127.0.0.1', '2', '99', 'Uuhyk', '2013-02-22 17:42:31');
INSERT INTO `character_history` VALUES ('291', '2', '1', '127.0.0.1', '1', '99', 'Uuhyk', '2013-02-22 17:51:40');
INSERT INTO `character_history` VALUES ('292', '2', '1', '127.0.0.1', '2', '99', 'Uuhyk', '2013-02-22 17:52:16');
INSERT INTO `character_history` VALUES ('293', '2', '1', '127.0.0.1', '4', '100', 'Ssd', '2013-02-22 23:41:45');
INSERT INTO `character_history` VALUES ('294', '2', '1', '127.0.0.1', '1', '100', 'Ssd', '2013-02-22 23:42:04');
INSERT INTO `character_history` VALUES ('295', '2', '1', '127.0.0.1', '2', '100', 'Ssd', '2013-02-22 23:46:15');
INSERT INTO `character_history` VALUES ('296', '2', '1', '127.0.0.1', '1', '100', 'Ssd', '2013-02-23 00:07:24');
INSERT INTO `character_history` VALUES ('297', '2', '1', '127.0.0.1', '2', '100', 'Ssd', '2013-02-23 00:07:55');
INSERT INTO `character_history` VALUES ('298', '2', '1', '127.0.0.1', '1', '100', 'Ssd', '2013-02-23 00:19:30');
INSERT INTO `character_history` VALUES ('299', '2', '1', '127.0.0.1', '2', '100', 'Ssd', '2013-02-23 00:23:21');
INSERT INTO `character_history` VALUES ('300', '2', '1', '127.0.0.1', '1', '99', 'Uuhyk', '2013-02-23 15:32:15');
INSERT INTO `character_history` VALUES ('301', '2', '1', '127.0.0.1', '2', '99', 'Uuhyk', '2013-02-23 15:33:18');
INSERT INTO `character_history` VALUES ('302', '2', '1', '127.0.0.1', '1', '99', 'Uuhyk', '2013-02-23 15:37:19');
INSERT INTO `character_history` VALUES ('303', '2', '1', '127.0.0.1', '2', '99', 'Uuhyk', '2013-02-23 15:39:49');
INSERT INTO `character_history` VALUES ('304', '2', '1', '127.0.0.1', '1', '100', 'Ssd', '2013-02-23 15:43:31');
INSERT INTO `character_history` VALUES ('305', '2', '1', '127.0.0.1', '2', '100', 'Ssd', '2013-02-23 15:43:39');
INSERT INTO `character_history` VALUES ('306', '2', '1', '127.0.0.1', '1', '98', 'Rgeg', '2013-02-23 15:44:01');
INSERT INTO `character_history` VALUES ('307', '2', '1', '127.0.0.1', '2', '98', 'Rgeg', '2013-02-23 15:45:04');
INSERT INTO `character_history` VALUES ('308', '2', '1', '127.0.0.1', '1', '98', 'Rgeg', '2013-02-23 19:58:02');
INSERT INTO `character_history` VALUES ('309', '2', '1', '127.0.0.1', '2', '98', 'Rgeg', '2013-02-23 19:58:37');
INSERT INTO `character_history` VALUES ('310', '2', '1', '127.0.0.1', '4', '101', 'Rdv', '2013-02-23 20:02:03');
INSERT INTO `character_history` VALUES ('311', '2', '1', '127.0.0.1', '1', '101', 'Rdv', '2013-02-23 20:02:20');
INSERT INTO `character_history` VALUES ('312', '2', '1', '127.0.0.1', '2', '101', 'Rdv', '2013-02-23 20:03:57');
INSERT INTO `character_history` VALUES ('313', '2', '1', '127.0.0.1', '1', '101', 'Rdv', '2013-02-23 20:37:49');
INSERT INTO `character_history` VALUES ('314', '2', '1', '127.0.0.1', '1', '101', 'Rdv', '2013-02-23 20:54:04');
INSERT INTO `character_history` VALUES ('315', '2', '1', '127.0.0.1', '2', '101', 'Rdv', '2013-02-23 20:55:28');
INSERT INTO `character_history` VALUES ('316', '2', '1', '127.0.0.1', '1', '101', 'Rdv', '2013-02-24 21:28:36');
INSERT INTO `character_history` VALUES ('317', '2', '1', '127.0.0.1', '2', '101', 'Rdv', '2013-02-24 21:29:30');
INSERT INTO `character_history` VALUES ('318', '2', '1', '127.0.0.1', '1', '101', 'Rdv', '2013-02-24 21:32:48');
INSERT INTO `character_history` VALUES ('319', '2', '1', '127.0.0.1', '2', '101', 'Rdv', '2013-02-24 21:33:31');
INSERT INTO `character_history` VALUES ('320', '2', '1', '127.0.0.1', '1', '100', 'Ssd', '2013-02-24 21:37:33');
INSERT INTO `character_history` VALUES ('321', '2', '1', '127.0.0.1', '2', '100', 'Ssd', '2013-02-24 21:38:28');
INSERT INTO `character_history` VALUES ('322', '2', '1', '127.0.0.1', '1', '101', 'Rdv', '2013-02-24 21:38:39');
INSERT INTO `character_history` VALUES ('323', '2', '1', '127.0.0.1', '2', '101', 'Rdv', '2013-02-24 21:38:53');
INSERT INTO `character_history` VALUES ('324', '2', '1', '127.0.0.1', '1', '101', 'Rdv', '2013-02-24 21:42:14');
INSERT INTO `character_history` VALUES ('325', '2', '1', '127.0.0.1', '2', '101', 'Rdv', '2013-02-24 21:43:19');
INSERT INTO `character_history` VALUES ('326', '2', '1', '127.0.0.1', '1', '101', 'Rdv', '2013-02-25 16:34:59');
INSERT INTO `character_history` VALUES ('327', '2', '1', '127.0.0.1', '2', '101', 'Rdv', '2013-02-25 16:43:07');
INSERT INTO `character_history` VALUES ('328', '2', '1', '127.0.0.1', '1', '72', 'Мма', '2013-02-25 18:14:18');
INSERT INTO `character_history` VALUES ('329', '2', '1', '127.0.0.1', '2', '72', 'Мма', '2013-02-25 18:25:59');
INSERT INTO `character_history` VALUES ('330', '2', '1', '127.0.0.1', '1', '72', 'Мма', '2013-02-27 20:39:51');
INSERT INTO `character_history` VALUES ('331', '2', '1', '127.0.0.1', '2', '72', 'Мма', '2013-02-27 20:41:34');
INSERT INTO `character_history` VALUES ('332', '2', '1', '127.0.0.1', '3', '101', 'Rdv', '2013-02-27 20:41:48');
INSERT INTO `character_history` VALUES ('333', '2', '1', '127.0.0.1', '4', '102', 'Апке', '2013-02-27 20:41:59');
INSERT INTO `character_history` VALUES ('334', '2', '1', '127.0.0.1', '1', '102', 'Апке', '2013-02-27 20:42:13');
INSERT INTO `character_history` VALUES ('335', '2', '1', '127.0.0.1', '2', '102', 'Апке', '2013-02-27 20:42:35');
INSERT INTO `character_history` VALUES ('336', '2', '1', '127.0.0.1', '1', '72', 'Мма', '2013-02-28 22:10:18');
INSERT INTO `character_history` VALUES ('337', '2', '1', '127.0.0.1', '2', '72', 'Мма', '2013-02-28 22:15:33');
INSERT INTO `character_history` VALUES ('338', '2', '1', '127.0.0.1', '1', '72', 'Мма', '2013-02-28 22:15:41');
INSERT INTO `character_history` VALUES ('339', '2', '1', '127.0.0.1', '2', '72', 'Мма', '2013-02-28 22:58:27');
INSERT INTO `character_history` VALUES ('340', '2', '1', '127.0.0.1', '1', '72', 'Мма', '2013-03-03 11:51:02');
INSERT INTO `character_history` VALUES ('341', '2', '1', '127.0.0.1', '2', '72', 'Мма', '2013-03-03 11:52:23');
INSERT INTO `character_history` VALUES ('342', '2', '1', '127.0.0.1', '3', '102', 'Апке', '2013-03-03 18:33:24');
INSERT INTO `character_history` VALUES ('343', '2', '1', '127.0.0.1', '4', '103', 'Пукпут', '2013-03-03 18:33:53');
INSERT INTO `character_history` VALUES ('344', '2', '1', '127.0.0.1', '1', '103', 'Пукпут', '2013-03-03 18:34:19');
INSERT INTO `character_history` VALUES ('345', '3', '1', '127.0.0.1', '1', '42', 'Ёж', '2013-03-03 18:43:45');
INSERT INTO `character_history` VALUES ('346', '3', '1', '127.0.0.1', '2', '42', 'Ёж', '2013-03-03 18:47:07');
INSERT INTO `character_history` VALUES ('347', '10', '1', '127.0.0.1', '4', '104', 'Gtyg', '2013-03-03 18:47:37');
INSERT INTO `character_history` VALUES ('348', '10', '1', '127.0.0.1', '1', '104', 'Gtyg', '2013-03-03 18:47:50');
INSERT INTO `character_history` VALUES ('349', '10', '1', '127.0.0.1', '2', '104', 'Gtyg', '2013-03-03 18:50:35');
INSERT INTO `character_history` VALUES ('350', '2', '1', '127.0.0.1', '2', '103', 'Пукпут', '2013-03-03 18:50:39');
INSERT INTO `character_history` VALUES ('351', '2', '1', '127.0.0.1', '1', '72', 'Мма', '2013-03-03 23:04:15');
INSERT INTO `character_history` VALUES ('352', '2', '1', '127.0.0.1', '2', '72', 'Мма', '2013-03-03 23:13:18');
INSERT INTO `character_history` VALUES ('353', '2', '1', '127.0.0.1', '1', '72', 'Мма', '2013-03-03 23:14:35');
INSERT INTO `character_history` VALUES ('354', '2', '1', '127.0.0.1', '2', '72', 'Мма', '2013-03-03 23:19:47');
INSERT INTO `character_history` VALUES ('355', '2', '1', '127.0.0.1', '1', '72', 'Мма', '2013-03-03 23:55:39');
INSERT INTO `character_history` VALUES ('356', '2', '1', '127.0.0.1', '2', '72', 'Мма', '2013-03-03 23:57:10');
INSERT INTO `character_history` VALUES ('357', '2', '1', '127.0.0.1', '1', '72', 'Мма', '2013-03-04 00:07:20');
INSERT INTO `character_history` VALUES ('358', '2', '1', '127.0.0.1', '2', '72', 'Мма', '2013-03-04 00:08:16');
INSERT INTO `character_history` VALUES ('359', '2', '1', '127.0.0.1', '1', '72', 'Мма', '2013-03-07 21:56:40');
INSERT INTO `character_history` VALUES ('360', '2', '1', '127.0.0.1', '2', '72', 'Мма', '2013-03-07 22:02:20');
INSERT INTO `character_history` VALUES ('361', '2', '1', '127.0.0.1', '1', '103', 'Пукпут', '2013-03-07 22:08:58');
INSERT INTO `character_history` VALUES ('362', '2', '1', '127.0.0.1', '2', '103', 'Пукпут', '2013-03-07 22:11:37');
INSERT INTO `character_history` VALUES ('363', '2', '1', '127.0.0.1', '1', '103', 'Пукпут', '2013-03-09 12:01:59');
INSERT INTO `character_history` VALUES ('364', '2', '1', '127.0.0.1', '2', '103', 'Пукпут', '2013-03-09 12:02:27');
INSERT INTO `character_history` VALUES ('365', '2', '1', '127.0.0.1', '1', '103', 'Пукпут', '2013-03-09 12:02:45');
INSERT INTO `character_history` VALUES ('366', '2', '1', '127.0.0.1', '2', '103', 'Пукпут', '2013-03-09 12:04:01');
INSERT INTO `character_history` VALUES ('367', '2', '1', '127.0.0.1', '1', '103', 'Пукпут', '2013-03-09 12:06:07');
INSERT INTO `character_history` VALUES ('368', '2', '1', '127.0.0.1', '2', '103', 'Пукпут', '2013-03-09 12:06:41');
INSERT INTO `character_history` VALUES ('369', '2', '1', '127.0.0.1', '3', '100', 'Ssd', '2013-03-12 20:00:34');
INSERT INTO `character_history` VALUES ('370', '2', '1', '127.0.0.1', '4', '105', 'Прг', '2013-03-12 20:01:15');
INSERT INTO `character_history` VALUES ('371', '2', '1', '127.0.0.1', '1', '105', 'Прг', '2013-03-12 20:01:41');
INSERT INTO `character_history` VALUES ('372', '2', '1', '127.0.0.1', '2', '105', 'Прг', '2013-03-12 20:03:01');
INSERT INTO `character_history` VALUES ('373', '2', '1', '127.0.0.1', '3', '105', 'Прг', '2013-03-12 20:03:12');
INSERT INTO `character_history` VALUES ('374', '2', '1', '127.0.0.1', '4', '106', 'Ерк', '2013-03-12 20:04:03');
INSERT INTO `character_history` VALUES ('375', '2', '1', '127.0.0.1', '1', '106', 'Ерк', '2013-03-12 20:04:12');
INSERT INTO `character_history` VALUES ('376', '2', '1', '127.0.0.1', '2', '106', 'Ерк', '2013-03-12 20:06:53');
INSERT INTO `character_history` VALUES ('377', '2', '1', '127.0.0.1', '3', '106', 'Ерк', '2013-03-12 22:42:43');
INSERT INTO `character_history` VALUES ('378', '2', '1', '127.0.0.1', '4', '107', 'Кеак', '2013-03-12 22:42:53');
INSERT INTO `character_history` VALUES ('379', '2', '1', '127.0.0.1', '1', '107', 'Кеак', '2013-03-12 22:43:13');
INSERT INTO `character_history` VALUES ('380', '2', '1', '127.0.0.1', '2', '107', 'Кеак', '2013-03-12 22:44:59');
INSERT INTO `character_history` VALUES ('381', '2', '1', '127.0.0.1', '1', '107', 'Кеак', '2013-03-12 22:48:03');
INSERT INTO `character_history` VALUES ('382', '2', '1', '127.0.0.1', '2', '107', 'Кеак', '2013-03-12 22:48:09');
INSERT INTO `character_history` VALUES ('383', '2', '1', '127.0.0.1', '3', '107', 'Кеак', '2013-03-12 22:48:18');
INSERT INTO `character_history` VALUES ('384', '2', '1', '127.0.0.1', '4', '108', 'Пввамв', '2013-03-12 22:48:32');
INSERT INTO `character_history` VALUES ('385', '2', '1', '127.0.0.1', '1', '108', 'Пввамв', '2013-03-12 22:48:39');
INSERT INTO `character_history` VALUES ('386', '2', '1', '127.0.0.1', '2', '108', 'Пввамв', '2013-03-12 22:48:47');
INSERT INTO `character_history` VALUES ('387', '2', '1', '127.0.0.1', '3', '108', 'Пввамв', '2013-03-12 22:52:19');
INSERT INTO `character_history` VALUES ('388', '2', '1', '127.0.0.1', '4', '109', 'Пвавпекпек', '2013-03-12 22:52:45');
INSERT INTO `character_history` VALUES ('389', '2', '1', '127.0.0.1', '1', '109', 'Пвавпекпек', '2013-03-12 22:52:55');
INSERT INTO `character_history` VALUES ('390', '2', '1', '127.0.0.1', '2', '109', 'Пвавпекпек', '2013-03-12 22:59:53');
INSERT INTO `character_history` VALUES ('391', '2', '1', '127.0.0.1', '3', '109', 'Пвавпекпек', '2013-03-21 21:43:45');
INSERT INTO `character_history` VALUES ('392', '2', '1', '127.0.0.1', '4', '110', 'Pristi', '2013-03-21 21:45:03');
INSERT INTO `character_history` VALUES ('393', '2', '1', '127.0.0.1', '1', '110', 'Pristi', '2013-03-21 21:45:28');
INSERT INTO `character_history` VALUES ('394', '2', '1', '127.0.0.1', '2', '110', 'Pristi', '2013-03-21 21:53:34');
INSERT INTO `character_history` VALUES ('395', '2', '1', '127.0.0.1', '1', '110', 'Pristi', '2013-03-31 22:02:16');
INSERT INTO `character_history` VALUES ('396', '2', '1', '127.0.0.1', '2', '110', 'Pristi', '2013-03-31 22:08:18');
INSERT INTO `character_history` VALUES ('397', '2', '1', '127.0.0.1', '1', '72', 'Мма', '2013-04-01 23:25:24');
INSERT INTO `character_history` VALUES ('398', '2', '1', '127.0.0.1', '2', '72', 'Мма', '2013-04-02 00:12:58');
INSERT INTO `character_history` VALUES ('399', '2', '1', '127.0.0.1', '1', '110', 'Pristi', '2013-04-05 20:59:19');
INSERT INTO `character_history` VALUES ('400', '3', '1', '127.0.0.1', '1', '42', 'Ёж', '2013-04-05 21:08:19');
INSERT INTO `character_history` VALUES ('401', '3', '1', '127.0.0.1', '2', '42', 'Ёж', '2013-04-05 21:09:26');
INSERT INTO `character_history` VALUES ('402', '2', '1', '127.0.0.1', '2', '110', 'Pristi', '2013-04-05 21:22:23');
INSERT INTO `character_history` VALUES ('403', '2', '1', '127.0.0.1', '1', '110', 'Pristi', '2013-04-05 21:22:31');
INSERT INTO `character_history` VALUES ('404', '2', '1', '127.0.0.1', '2', '110', 'Pristi', '2013-04-05 21:33:42');
INSERT INTO `character_history` VALUES ('405', '2', '1', '127.0.0.1', '1', '103', 'Пукпут', '2013-04-05 23:01:50');
INSERT INTO `character_history` VALUES ('406', '2', '1', '127.0.0.1', '2', '103', 'Пукпут', '2013-04-05 23:07:45');
INSERT INTO `character_history` VALUES ('407', '2', '1', '127.0.0.1', '1', '103', 'Пукпут', '2013-04-06 12:35:17');
INSERT INTO `character_history` VALUES ('408', '2', '1', '127.0.0.1', '2', '103', 'Пукпут', '2013-04-06 13:19:15');
INSERT INTO `character_history` VALUES ('409', '2', '1', '127.0.0.1', '1', '103', 'Пукпут', '2013-04-06 15:23:58');
INSERT INTO `character_history` VALUES ('410', '2', '1', '127.0.0.1', '2', '103', 'Пукпут', '2013-04-06 15:38:28');
INSERT INTO `character_history` VALUES ('411', '2', '1', '127.0.0.1', '1', '103', 'Пукпут', '2013-04-06 19:55:14');
INSERT INTO `character_history` VALUES ('412', '2', '1', '127.0.0.1', '2', '103', 'Пукпут', '2013-04-06 19:56:38');
INSERT INTO `character_history` VALUES ('413', '2', '1', '127.0.0.1', '1', '103', 'Пукпут', '2013-04-06 22:29:50');
INSERT INTO `character_history` VALUES ('414', '2', '1', '127.0.0.1', '2', '103', 'Пукпут', '2013-04-06 22:30:12');
INSERT INTO `character_history` VALUES ('415', '2', '1', '127.0.0.1', '1', '103', 'Пукпут', '2013-04-06 23:20:12');
INSERT INTO `character_history` VALUES ('416', '2', '1', '127.0.0.1', '2', '103', 'Пукпут', '2013-04-06 23:23:16');
INSERT INTO `character_history` VALUES ('417', '2', '1', '127.0.0.1', '1', '103', 'Пукпут', '2013-04-06 23:23:25');
INSERT INTO `character_history` VALUES ('418', '2', '1', '127.0.0.1', '2', '103', 'Пукпут', '2013-04-06 23:38:18');
INSERT INTO `character_history` VALUES ('419', '2', '1', '127.0.0.1', '1', '103', 'Пукпут', '2013-04-06 23:38:27');
INSERT INTO `character_history` VALUES ('420', '2', '1', '127.0.0.1', '2', '103', 'Пукпут', '2013-04-07 00:11:24');
INSERT INTO `character_history` VALUES ('421', '2', '1', '127.0.0.1', '1', '103', 'Пукпут', '2013-04-07 13:20:55');
INSERT INTO `character_history` VALUES ('422', '2', '1', '127.0.0.1', '2', '103', 'Пукпут', '2013-04-07 13:23:09');
INSERT INTO `character_history` VALUES ('423', '2', '1', '127.0.0.1', '1', '103', 'Пукпут', '2013-04-07 13:27:47');
INSERT INTO `character_history` VALUES ('424', '2', '1', '127.0.0.1', '2', '103', 'Пукпут', '2013-04-07 13:28:38');
INSERT INTO `character_history` VALUES ('425', '2', '1', '127.0.0.1', '1', '103', 'Пукпут', '2013-04-08 23:13:34');
INSERT INTO `character_history` VALUES ('426', '2', '1', '127.0.0.1', '1', '103', 'Пукпут', '2013-04-09 00:03:23');
INSERT INTO `character_history` VALUES ('427', '2', '1', '127.0.0.1', '2', '103', 'Пукпут', '2013-04-09 00:22:00');
INSERT INTO `character_history` VALUES ('428', '2', '1', '127.0.0.1', '1', '103', 'Пукпут', '2013-04-09 00:22:59');

-- ----------------------------
-- Table structure for `ip_banned`
-- ----------------------------
DROP TABLE IF EXISTS `ip_banned`;
CREATE TABLE `ip_banned` (
  `ip` varchar(15) NOT NULL DEFAULT '127.0.0.1',
  `bandate` int(10) unsigned NOT NULL,
  `unbandate` int(10) unsigned NOT NULL,
  `bannedby` varchar(50) NOT NULL DEFAULT '[Console]',
  `banreason` varchar(255) NOT NULL DEFAULT 'no reason',
  PRIMARY KEY (`ip`,`bandate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Banned IPs';

-- ----------------------------
-- Records of ip_banned
-- ----------------------------

-- ----------------------------
-- Table structure for `logs`
-- ----------------------------
DROP TABLE IF EXISTS `logs`;
CREATE TABLE `logs` (
  `time` int(10) unsigned NOT NULL,
  `realm` int(10) unsigned NOT NULL,
  `type` tinyint(3) unsigned NOT NULL,
  `level` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `string` text CHARACTER SET latin1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of logs
-- ----------------------------

-- ----------------------------
-- Table structure for `rbac_account_groups`
-- ----------------------------
DROP TABLE IF EXISTS `rbac_account_groups`;
CREATE TABLE `rbac_account_groups` (
  `accountId` int(10) unsigned NOT NULL COMMENT 'Account id',
  `groupId` int(10) unsigned NOT NULL COMMENT 'Group id',
  `realmId` int(11) NOT NULL DEFAULT '-1' COMMENT 'Realm Id, -1 means all',
  PRIMARY KEY (`accountId`,`groupId`,`realmId`),
  KEY `fk__rbac_account_groups__rbac_groups` (`groupId`),
  CONSTRAINT `fk__rbac_account_groups__account` FOREIGN KEY (`accountId`) REFERENCES `account` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk__rbac_account_groups__rbac_groups` FOREIGN KEY (`groupId`) REFERENCES `rbac_groups` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Account-Group relation';

-- ----------------------------
-- Records of rbac_account_groups
-- ----------------------------
INSERT INTO `rbac_account_groups` VALUES ('1', '1', '-1');
INSERT INTO `rbac_account_groups` VALUES ('2', '1', '-1');
INSERT INTO `rbac_account_groups` VALUES ('3', '1', '-1');
INSERT INTO `rbac_account_groups` VALUES ('4', '1', '-1');
INSERT INTO `rbac_account_groups` VALUES ('5', '1', '-1');
INSERT INTO `rbac_account_groups` VALUES ('6', '1', '-1');
INSERT INTO `rbac_account_groups` VALUES ('7', '1', '-1');
INSERT INTO `rbac_account_groups` VALUES ('8', '1', '-1');
INSERT INTO `rbac_account_groups` VALUES ('9', '1', '-1');
INSERT INTO `rbac_account_groups` VALUES ('10', '1', '-1');
INSERT INTO `rbac_account_groups` VALUES ('11', '1', '-1');
INSERT INTO `rbac_account_groups` VALUES ('12', '1', '-1');
INSERT INTO `rbac_account_groups` VALUES ('2', '2', '-1');
INSERT INTO `rbac_account_groups` VALUES ('3', '2', '1');
INSERT INTO `rbac_account_groups` VALUES ('6', '2', '-1');
INSERT INTO `rbac_account_groups` VALUES ('9', '2', '-1');
INSERT INTO `rbac_account_groups` VALUES ('11', '2', '-1');
INSERT INTO `rbac_account_groups` VALUES ('12', '2', '-1');
INSERT INTO `rbac_account_groups` VALUES ('2', '3', '-1');
INSERT INTO `rbac_account_groups` VALUES ('3', '3', '1');
INSERT INTO `rbac_account_groups` VALUES ('6', '3', '-1');
INSERT INTO `rbac_account_groups` VALUES ('9', '3', '-1');
INSERT INTO `rbac_account_groups` VALUES ('11', '3', '-1');
INSERT INTO `rbac_account_groups` VALUES ('12', '3', '-1');
INSERT INTO `rbac_account_groups` VALUES ('2', '4', '-1');
INSERT INTO `rbac_account_groups` VALUES ('3', '4', '1');
INSERT INTO `rbac_account_groups` VALUES ('6', '4', '-1');
INSERT INTO `rbac_account_groups` VALUES ('9', '4', '-1');
INSERT INTO `rbac_account_groups` VALUES ('11', '4', '-1');
INSERT INTO `rbac_account_groups` VALUES ('12', '4', '-1');

-- ----------------------------
-- Table structure for `rbac_account_permissions`
-- ----------------------------
DROP TABLE IF EXISTS `rbac_account_permissions`;
CREATE TABLE `rbac_account_permissions` (
  `accountId` int(10) unsigned NOT NULL COMMENT 'Account id',
  `permissionId` int(10) unsigned NOT NULL COMMENT 'Permission id',
  `granted` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Granted = 1, Denied = 0',
  `realmId` int(11) NOT NULL DEFAULT '-1' COMMENT 'Realm Id, -1 means all',
  PRIMARY KEY (`accountId`,`permissionId`,`realmId`),
  KEY `fk__rbac_account_roles__rbac_permissions` (`permissionId`),
  CONSTRAINT `fk__rbac_account_permissions__account` FOREIGN KEY (`accountId`) REFERENCES `account` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk__rbac_account_roles__rbac_permissions` FOREIGN KEY (`permissionId`) REFERENCES `rbac_permissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Account-Permission relation';

-- ----------------------------
-- Records of rbac_account_permissions
-- ----------------------------

-- ----------------------------
-- Table structure for `rbac_account_roles`
-- ----------------------------
DROP TABLE IF EXISTS `rbac_account_roles`;
CREATE TABLE `rbac_account_roles` (
  `accountId` int(10) unsigned NOT NULL COMMENT 'Account id',
  `roleId` int(10) unsigned NOT NULL COMMENT 'Role id',
  `granted` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Granted = 1, Denied = 0',
  `realmId` int(11) NOT NULL DEFAULT '-1' COMMENT 'Realm Id, -1 means all',
  PRIMARY KEY (`accountId`,`roleId`,`realmId`),
  KEY `fk__rbac_account_roles__rbac_roles` (`roleId`),
  CONSTRAINT `fk__rbac_account_roles__account` FOREIGN KEY (`accountId`) REFERENCES `account` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk__rbac_account_roles__rbac_roles` FOREIGN KEY (`roleId`) REFERENCES `rbac_roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Account-Role relation';

-- ----------------------------
-- Records of rbac_account_roles
-- ----------------------------

-- ----------------------------
-- Table structure for `rbac_groups`
-- ----------------------------
DROP TABLE IF EXISTS `rbac_groups`;
CREATE TABLE `rbac_groups` (
  `id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Group id',
  `name` varchar(50) NOT NULL COMMENT 'Group name',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Group List';

-- ----------------------------
-- Records of rbac_groups
-- ----------------------------
INSERT INTO `rbac_groups` VALUES ('1', 'Player');
INSERT INTO `rbac_groups` VALUES ('2', 'Moderator');
INSERT INTO `rbac_groups` VALUES ('3', 'GameMaster');
INSERT INTO `rbac_groups` VALUES ('4', 'Administrator');

-- ----------------------------
-- Table structure for `rbac_group_roles`
-- ----------------------------
DROP TABLE IF EXISTS `rbac_group_roles`;
CREATE TABLE `rbac_group_roles` (
  `groupId` int(10) unsigned NOT NULL COMMENT 'group id',
  `roleId` int(10) unsigned NOT NULL COMMENT 'Role id',
  PRIMARY KEY (`groupId`,`roleId`),
  KEY `fk__rbac_group_roles__rbac_roles` (`roleId`),
  CONSTRAINT `fk__rbac_group_roles__rbac_groups` FOREIGN KEY (`groupId`) REFERENCES `rbac_groups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk__rbac_group_roles__rbac_roles` FOREIGN KEY (`roleId`) REFERENCES `rbac_roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Group Role relation';

-- ----------------------------
-- Records of rbac_group_roles
-- ----------------------------
INSERT INTO `rbac_group_roles` VALUES ('1', '1');
INSERT INTO `rbac_group_roles` VALUES ('2', '2');
INSERT INTO `rbac_group_roles` VALUES ('3', '3');
INSERT INTO `rbac_group_roles` VALUES ('4', '4');
INSERT INTO `rbac_group_roles` VALUES ('2', '5');
INSERT INTO `rbac_group_roles` VALUES ('1', '6');
INSERT INTO `rbac_group_roles` VALUES ('1', '7');
INSERT INTO `rbac_group_roles` VALUES ('2', '8');
INSERT INTO `rbac_group_roles` VALUES ('3', '8');
INSERT INTO `rbac_group_roles` VALUES ('4', '8');
INSERT INTO `rbac_group_roles` VALUES ('2', '9');
INSERT INTO `rbac_group_roles` VALUES ('3', '9');
INSERT INTO `rbac_group_roles` VALUES ('4', '9');
INSERT INTO `rbac_group_roles` VALUES ('2', '10');
INSERT INTO `rbac_group_roles` VALUES ('3', '10');
INSERT INTO `rbac_group_roles` VALUES ('4', '10');
INSERT INTO `rbac_group_roles` VALUES ('2', '11');
INSERT INTO `rbac_group_roles` VALUES ('3', '11');
INSERT INTO `rbac_group_roles` VALUES ('4', '11');
INSERT INTO `rbac_group_roles` VALUES ('2', '12');
INSERT INTO `rbac_group_roles` VALUES ('3', '12');
INSERT INTO `rbac_group_roles` VALUES ('4', '12');
INSERT INTO `rbac_group_roles` VALUES ('2', '13');
INSERT INTO `rbac_group_roles` VALUES ('3', '13');
INSERT INTO `rbac_group_roles` VALUES ('4', '13');
INSERT INTO `rbac_group_roles` VALUES ('2', '14');
INSERT INTO `rbac_group_roles` VALUES ('3', '14');
INSERT INTO `rbac_group_roles` VALUES ('4', '14');
INSERT INTO `rbac_group_roles` VALUES ('2', '15');
INSERT INTO `rbac_group_roles` VALUES ('3', '15');
INSERT INTO `rbac_group_roles` VALUES ('4', '15');
INSERT INTO `rbac_group_roles` VALUES ('2', '16');
INSERT INTO `rbac_group_roles` VALUES ('3', '16');
INSERT INTO `rbac_group_roles` VALUES ('4', '16');
INSERT INTO `rbac_group_roles` VALUES ('2', '17');
INSERT INTO `rbac_group_roles` VALUES ('3', '17');
INSERT INTO `rbac_group_roles` VALUES ('4', '17');
INSERT INTO `rbac_group_roles` VALUES ('4', '18');
INSERT INTO `rbac_group_roles` VALUES ('2', '19');
INSERT INTO `rbac_group_roles` VALUES ('3', '19');
INSERT INTO `rbac_group_roles` VALUES ('4', '19');
INSERT INTO `rbac_group_roles` VALUES ('2', '20');
INSERT INTO `rbac_group_roles` VALUES ('3', '20');
INSERT INTO `rbac_group_roles` VALUES ('4', '20');
INSERT INTO `rbac_group_roles` VALUES ('2', '21');
INSERT INTO `rbac_group_roles` VALUES ('3', '21');
INSERT INTO `rbac_group_roles` VALUES ('4', '21');
INSERT INTO `rbac_group_roles` VALUES ('2', '22');
INSERT INTO `rbac_group_roles` VALUES ('3', '22');
INSERT INTO `rbac_group_roles` VALUES ('4', '22');
INSERT INTO `rbac_group_roles` VALUES ('4', '23');
INSERT INTO `rbac_group_roles` VALUES ('2', '24');
INSERT INTO `rbac_group_roles` VALUES ('3', '24');
INSERT INTO `rbac_group_roles` VALUES ('4', '24');
INSERT INTO `rbac_group_roles` VALUES ('2', '25');
INSERT INTO `rbac_group_roles` VALUES ('3', '25');
INSERT INTO `rbac_group_roles` VALUES ('4', '25');
INSERT INTO `rbac_group_roles` VALUES ('2', '26');
INSERT INTO `rbac_group_roles` VALUES ('3', '26');
INSERT INTO `rbac_group_roles` VALUES ('4', '26');
INSERT INTO `rbac_group_roles` VALUES ('2', '27');
INSERT INTO `rbac_group_roles` VALUES ('3', '27');
INSERT INTO `rbac_group_roles` VALUES ('4', '27');
INSERT INTO `rbac_group_roles` VALUES ('2', '28');
INSERT INTO `rbac_group_roles` VALUES ('3', '28');
INSERT INTO `rbac_group_roles` VALUES ('4', '28');
INSERT INTO `rbac_group_roles` VALUES ('2', '29');
INSERT INTO `rbac_group_roles` VALUES ('3', '29');
INSERT INTO `rbac_group_roles` VALUES ('4', '29');
INSERT INTO `rbac_group_roles` VALUES ('2', '30');
INSERT INTO `rbac_group_roles` VALUES ('3', '30');
INSERT INTO `rbac_group_roles` VALUES ('4', '30');
INSERT INTO `rbac_group_roles` VALUES ('2', '32');
INSERT INTO `rbac_group_roles` VALUES ('3', '32');
INSERT INTO `rbac_group_roles` VALUES ('4', '32');
INSERT INTO `rbac_group_roles` VALUES ('2', '35');
INSERT INTO `rbac_group_roles` VALUES ('3', '35');
INSERT INTO `rbac_group_roles` VALUES ('4', '35');
INSERT INTO `rbac_group_roles` VALUES ('2', '36');
INSERT INTO `rbac_group_roles` VALUES ('3', '36');
INSERT INTO `rbac_group_roles` VALUES ('4', '36');
INSERT INTO `rbac_group_roles` VALUES ('2', '37');
INSERT INTO `rbac_group_roles` VALUES ('3', '37');
INSERT INTO `rbac_group_roles` VALUES ('4', '37');
INSERT INTO `rbac_group_roles` VALUES ('2', '38');
INSERT INTO `rbac_group_roles` VALUES ('3', '38');
INSERT INTO `rbac_group_roles` VALUES ('4', '38');

-- ----------------------------
-- Table structure for `rbac_permissions`
-- ----------------------------
DROP TABLE IF EXISTS `rbac_permissions`;
CREATE TABLE `rbac_permissions` (
  `id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Permission id',
  `name` varchar(100) NOT NULL COMMENT 'Permission name',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Permission List';

-- ----------------------------
-- Records of rbac_permissions
-- ----------------------------
INSERT INTO `rbac_permissions` VALUES ('1', 'Instant logout');
INSERT INTO `rbac_permissions` VALUES ('2', 'Skip Queue');
INSERT INTO `rbac_permissions` VALUES ('3', 'Join Normal Battleground');
INSERT INTO `rbac_permissions` VALUES ('4', 'Join Random Battleground');
INSERT INTO `rbac_permissions` VALUES ('5', 'Join Arenas');
INSERT INTO `rbac_permissions` VALUES ('6', 'Join Dungeon Finder');
INSERT INTO `rbac_permissions` VALUES ('7', 'Player Commands (Temporal till commands moved to rbac)');
INSERT INTO `rbac_permissions` VALUES ('8', 'Moderator Commands (Temporal till commands moved to rbac)');
INSERT INTO `rbac_permissions` VALUES ('9', 'GameMaster Commands (Temporal till commands moved to rbac)');
INSERT INTO `rbac_permissions` VALUES ('10', 'Administrator Commands (Temporal till commands moved to rbac)');
INSERT INTO `rbac_permissions` VALUES ('11', 'Log GM trades');
INSERT INTO `rbac_permissions` VALUES ('13', 'Skip Instance required bosses check');
INSERT INTO `rbac_permissions` VALUES ('14', 'Skips character creation team mask check');
INSERT INTO `rbac_permissions` VALUES ('15', 'Skips character creation class mask check');
INSERT INTO `rbac_permissions` VALUES ('16', 'Skips character creation race mask check');
INSERT INTO `rbac_permissions` VALUES ('17', 'Skips character creation reserved name check');
INSERT INTO `rbac_permissions` VALUES ('18', 'Skips character creation heroic min level check');
INSERT INTO `rbac_permissions` VALUES ('19', 'Skips needed requirements to use channel check');
INSERT INTO `rbac_permissions` VALUES ('20', 'Skip disable map check');
INSERT INTO `rbac_permissions` VALUES ('21', 'Skip reset talents when used more than allowed check');
INSERT INTO `rbac_permissions` VALUES ('22', 'Skip spam chat check');
INSERT INTO `rbac_permissions` VALUES ('23', 'Skip over-speed ping check');
INSERT INTO `rbac_permissions` VALUES ('24', 'Creation of two side faction characters in same account');
INSERT INTO `rbac_permissions` VALUES ('25', 'Allow say chat between factions');
INSERT INTO `rbac_permissions` VALUES ('26', 'Allow channel chat between factions');
INSERT INTO `rbac_permissions` VALUES ('27', 'Two side mail interaction');
INSERT INTO `rbac_permissions` VALUES ('28', 'See two side who list');
INSERT INTO `rbac_permissions` VALUES ('29', 'Add friends of other faction');
INSERT INTO `rbac_permissions` VALUES ('30', 'Save character without delay with .save command');
INSERT INTO `rbac_permissions` VALUES ('31', 'Use params with .unstuck command');
INSERT INTO `rbac_permissions` VALUES ('32', 'Can be assigned tickets with .assign ticket command');
INSERT INTO `rbac_permissions` VALUES ('33', 'Notify if a command was not found');
INSERT INTO `rbac_permissions` VALUES ('34', 'Check if should appear in list using .gm ingame command');
INSERT INTO `rbac_permissions` VALUES ('35', 'See all security levels with who command');
INSERT INTO `rbac_permissions` VALUES ('36', 'Filter whispers');
INSERT INTO `rbac_permissions` VALUES ('37', 'Use staff badge in chat');
INSERT INTO `rbac_permissions` VALUES ('38', 'Resurrect with full Health Points');
INSERT INTO `rbac_permissions` VALUES ('39', 'Restore saved gm setting states');
INSERT INTO `rbac_permissions` VALUES ('40', 'Allows to add a gm to friend list');
INSERT INTO `rbac_permissions` VALUES ('41', 'Use Config option START_GM_LEVEL to assign new character level');
INSERT INTO `rbac_permissions` VALUES ('42', 'Allows to use CMSG_WORLD_TELEPORT opcode');
INSERT INTO `rbac_permissions` VALUES ('43', 'Allows to use CMSG_WHOIS opcode');
INSERT INTO `rbac_permissions` VALUES ('44', 'Receive global GM messages/texts');
INSERT INTO `rbac_permissions` VALUES ('45', 'Join channels without announce');
INSERT INTO `rbac_permissions` VALUES ('46', 'Change channel settings without being channel moderator');
INSERT INTO `rbac_permissions` VALUES ('47', 'Enables lower security than target check');

-- ----------------------------
-- Table structure for `rbac_roles`
-- ----------------------------
DROP TABLE IF EXISTS `rbac_roles`;
CREATE TABLE `rbac_roles` (
  `id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Role id',
  `name` varchar(70) NOT NULL COMMENT 'Role name',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Roles List';

-- ----------------------------
-- Records of rbac_roles
-- ----------------------------
INSERT INTO `rbac_roles` VALUES ('1', 'Player Commands');
INSERT INTO `rbac_roles` VALUES ('2', 'Moderator Commands');
INSERT INTO `rbac_roles` VALUES ('3', 'GameMaster Commands');
INSERT INTO `rbac_roles` VALUES ('4', 'Administrator Commands');
INSERT INTO `rbac_roles` VALUES ('5', 'Quick Login/Logout');
INSERT INTO `rbac_roles` VALUES ('6', 'Use Battleground/Arenas');
INSERT INTO `rbac_roles` VALUES ('7', 'Use Dungeon Finder');
INSERT INTO `rbac_roles` VALUES ('8', 'Log GM trades');
INSERT INTO `rbac_roles` VALUES ('9', 'Skip Instance required bosses check');
INSERT INTO `rbac_roles` VALUES ('10', 'Ticket management');
INSERT INTO `rbac_roles` VALUES ('11', 'Instant .save');
INSERT INTO `rbac_roles` VALUES ('12', 'Allow params with .unstuck');
INSERT INTO `rbac_roles` VALUES ('13', 'Full HP after resurrect');
INSERT INTO `rbac_roles` VALUES ('14', 'Appear in GM ingame list');
INSERT INTO `rbac_roles` VALUES ('15', 'Use staff badge in chat');
INSERT INTO `rbac_roles` VALUES ('16', 'Receive global GM messages/texts');
INSERT INTO `rbac_roles` VALUES ('17', 'Skip over-speed ping check');
INSERT INTO `rbac_roles` VALUES ('18', 'Allows Admin Opcodes');
INSERT INTO `rbac_roles` VALUES ('19', 'Two side mail interaction');
INSERT INTO `rbac_roles` VALUES ('20', 'Notify if a command was not found');
INSERT INTO `rbac_roles` VALUES ('21', 'Enables lower security than target check');
INSERT INTO `rbac_roles` VALUES ('22', 'Skip disable map check');
INSERT INTO `rbac_roles` VALUES ('23', 'Skip reset talents when used more than allowed check');
INSERT INTO `rbac_roles` VALUES ('24', 'Skip spam chat check');
INSERT INTO `rbac_roles` VALUES ('25', 'Restore saved gm setting states');
INSERT INTO `rbac_roles` VALUES ('26', 'Use Config option START_GM_LEVEL to assign new character level');
INSERT INTO `rbac_roles` VALUES ('27', 'Skips needed requirements to use channel check');
INSERT INTO `rbac_roles` VALUES ('28', 'Allow say chat between factions');
INSERT INTO `rbac_roles` VALUES ('29', 'Filter whispers');
INSERT INTO `rbac_roles` VALUES ('30', 'Allow channel chat between factions');
INSERT INTO `rbac_roles` VALUES ('31', 'Join channels without announce');
INSERT INTO `rbac_roles` VALUES ('32', 'Change channel settings without being channel moderator');
INSERT INTO `rbac_roles` VALUES ('35', 'See two side who list');
INSERT INTO `rbac_roles` VALUES ('36', 'Add friends of other faction');
INSERT INTO `rbac_roles` VALUES ('37', 'See all security levels with who command');
INSERT INTO `rbac_roles` VALUES ('38', 'Allows to add a gm to friend list');

-- ----------------------------
-- Table structure for `rbac_role_permissions`
-- ----------------------------
DROP TABLE IF EXISTS `rbac_role_permissions`;
CREATE TABLE `rbac_role_permissions` (
  `roleId` int(10) unsigned NOT NULL COMMENT 'Role id',
  `permissionId` int(10) unsigned NOT NULL COMMENT 'Permission id',
  PRIMARY KEY (`roleId`,`permissionId`),
  KEY `fk__role_permissions__rbac_permissions` (`permissionId`),
  CONSTRAINT `fk__role_permissions__rbac_permissions` FOREIGN KEY (`permissionId`) REFERENCES `rbac_permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk__role_permissions__rbac_roles` FOREIGN KEY (`roleId`) REFERENCES `rbac_roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Role Permission relation';

-- ----------------------------
-- Records of rbac_role_permissions
-- ----------------------------
INSERT INTO `rbac_role_permissions` VALUES ('5', '1');
INSERT INTO `rbac_role_permissions` VALUES ('5', '2');
INSERT INTO `rbac_role_permissions` VALUES ('6', '3');
INSERT INTO `rbac_role_permissions` VALUES ('6', '4');
INSERT INTO `rbac_role_permissions` VALUES ('6', '5');
INSERT INTO `rbac_role_permissions` VALUES ('7', '6');
INSERT INTO `rbac_role_permissions` VALUES ('1', '7');
INSERT INTO `rbac_role_permissions` VALUES ('2', '8');
INSERT INTO `rbac_role_permissions` VALUES ('3', '9');
INSERT INTO `rbac_role_permissions` VALUES ('4', '10');
INSERT INTO `rbac_role_permissions` VALUES ('8', '11');
INSERT INTO `rbac_role_permissions` VALUES ('9', '13');
INSERT INTO `rbac_role_permissions` VALUES ('17', '23');
INSERT INTO `rbac_role_permissions` VALUES ('19', '27');
INSERT INTO `rbac_role_permissions` VALUES ('10', '32');
INSERT INTO `rbac_role_permissions` VALUES ('16', '44');

-- ----------------------------
-- Table structure for `rbac_security_level_groups`
-- ----------------------------
DROP TABLE IF EXISTS `rbac_security_level_groups`;
CREATE TABLE `rbac_security_level_groups` (
  `secId` int(10) unsigned NOT NULL COMMENT 'Security Level id',
  `groupId` int(10) unsigned NOT NULL COMMENT 'group id',
  PRIMARY KEY (`secId`,`groupId`),
  KEY `fk__rbac_security_level_groups__rbac_groups` (`groupId`),
  CONSTRAINT `fk__rbac_security_level_groups__rbac_groups` FOREIGN KEY (`groupId`) REFERENCES `rbac_groups` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Default groups to assign when an account is set gm level';

-- ----------------------------
-- Records of rbac_security_level_groups
-- ----------------------------
INSERT INTO `rbac_security_level_groups` VALUES ('0', '1');
INSERT INTO `rbac_security_level_groups` VALUES ('1', '1');
INSERT INTO `rbac_security_level_groups` VALUES ('2', '1');
INSERT INTO `rbac_security_level_groups` VALUES ('3', '1');
INSERT INTO `rbac_security_level_groups` VALUES ('1', '2');
INSERT INTO `rbac_security_level_groups` VALUES ('2', '2');
INSERT INTO `rbac_security_level_groups` VALUES ('3', '2');
INSERT INTO `rbac_security_level_groups` VALUES ('2', '3');
INSERT INTO `rbac_security_level_groups` VALUES ('3', '3');
INSERT INTO `rbac_security_level_groups` VALUES ('3', '4');

-- ----------------------------
-- Table structure for `realmcharacters`
-- ----------------------------
DROP TABLE IF EXISTS `realmcharacters`;
CREATE TABLE `realmcharacters` (
  `realmid` int(10) unsigned NOT NULL DEFAULT '0',
  `acctid` int(10) unsigned NOT NULL,
  `numchars` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`realmid`,`acctid`),
  KEY `acctid` (`acctid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Realm Character Tracker';

-- ----------------------------
-- Records of realmcharacters
-- ----------------------------
INSERT INTO `realmcharacters` VALUES ('1', '1', '7');
INSERT INTO `realmcharacters` VALUES ('1', '2', '10');
INSERT INTO `realmcharacters` VALUES ('1', '3', '7');
INSERT INTO `realmcharacters` VALUES ('1', '4', '8');
INSERT INTO `realmcharacters` VALUES ('1', '5', '0');
INSERT INTO `realmcharacters` VALUES ('1', '6', '10');
INSERT INTO `realmcharacters` VALUES ('1', '7', '0');
INSERT INTO `realmcharacters` VALUES ('1', '8', '0');
INSERT INTO `realmcharacters` VALUES ('1', '9', '1');
INSERT INTO `realmcharacters` VALUES ('1', '10', '2');
INSERT INTO `realmcharacters` VALUES ('1', '11', '2');
INSERT INTO `realmcharacters` VALUES ('1', '12', '1');
INSERT INTO `realmcharacters` VALUES ('2', '2', '10');
INSERT INTO `realmcharacters` VALUES ('2', '6', '0');
INSERT INTO `realmcharacters` VALUES ('2', '7', '0');
INSERT INTO `realmcharacters` VALUES ('2', '8', '0');
INSERT INTO `realmcharacters` VALUES ('2', '9', '0');
INSERT INTO `realmcharacters` VALUES ('2', '10', '0');
INSERT INTO `realmcharacters` VALUES ('2', '11', '0');
INSERT INTO `realmcharacters` VALUES ('2', '12', '0');

-- ----------------------------
-- Table structure for `realmlist`
-- ----------------------------
DROP TABLE IF EXISTS `realmlist`;
CREATE TABLE `realmlist` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL DEFAULT '',
  `address` varchar(255) NOT NULL DEFAULT '127.0.0.1',
  `localAddress` varchar(255) NOT NULL DEFAULT '127.0.0.1',
  `localSubnetMask` varchar(255) NOT NULL DEFAULT '255.255.255.0',
  `port` smallint(5) unsigned NOT NULL DEFAULT '8085',
  `icon` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `flag` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `timezone` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `allowedSecurityLevel` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `population` float unsigned NOT NULL DEFAULT '0',
  `gamebuild` int(10) unsigned NOT NULL DEFAULT '12340',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='Realm System';

-- ----------------------------
-- Records of realmlist
-- ----------------------------
INSERT INTO `realmlist` VALUES ('1', 'ggg', 'wow-syros.dyndns.org', '0.0.0.0', '255.255.255.0', '8085', '0', '0', '1', '0', '0', '12340');
INSERT INTO `realmlist` VALUES ('2', 'аупк', '127.0.0.1', '0.0.0.0', '255.255.255.0', '8085', '0', '0', '1', '0', '0', '12340');

-- ----------------------------
-- Table structure for `uptime`
-- ----------------------------
DROP TABLE IF EXISTS `uptime`;
CREATE TABLE `uptime` (
  `realmid` int(10) unsigned NOT NULL,
  `starttime` int(10) unsigned NOT NULL DEFAULT '0',
  `uptime` int(10) unsigned NOT NULL DEFAULT '0',
  `maxplayers` smallint(5) unsigned NOT NULL DEFAULT '0',
  `revision` varchar(255) NOT NULL DEFAULT 'Trinitycore',
  PRIMARY KEY (`realmid`,`starttime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Uptime system';

-- ----------------------------
-- Records of uptime
-- ----------------------------
INSERT INTO `uptime` VALUES ('1', '1352134613', '6602', '0', 'TrinityCore rev. 2012-11-04 23:57:29 +0400 (9699954a2c0d) (Win32, Release)');
INSERT INTO `uptime` VALUES ('1', '1352142151', '2415', '0', 'TrinityCore rev. 2012-11-04 23:57:29 +0400 (9699954a2c0d) (Win32, Release)');
INSERT INTO `uptime` VALUES ('1', '1352148260', '0', '0', 'TrinityCore rev. 2012-11-05 23:40:32 +0400 (5c2395c59a2e+) (Win32, Release)');
INSERT INTO `uptime` VALUES ('1', '1352149696', '0', '0', 'TrinityCore rev. 2012-11-06 00:53:56 +0400 (1d5b2912d82e) (Win32, Release)');
INSERT INTO `uptime` VALUES ('1', '1352313739', '0', '0', 'TrinityCore rev. 2012-11-06 00:53:56 +0400 (1d5b2912d82e+) (Win32, Release)');
INSERT INTO `uptime` VALUES ('1', '1352323266', '0', '0', 'TrinityCore rev. 2012-11-06 00:53:56 +0400 (1d5b2912d82e+) (Win32, Release)');
INSERT INTO `uptime` VALUES ('1', '1352323487', '0', '0', 'TrinityCore rev. 2012-11-06 00:53:56 +0400 (1d5b2912d82e+) (Win32, Release)');
INSERT INTO `uptime` VALUES ('1', '1352323834', '0', '0', 'TrinityCore rev. 2012-11-06 00:53:56 +0400 (1d5b2912d82e+) (Win32, Release)');
INSERT INTO `uptime` VALUES ('1', '1352323943', '0', '0', 'TrinityCore rev. 2012-11-06 00:53:56 +0400 (1d5b2912d82e+) (Win32, Release)');
INSERT INTO `uptime` VALUES ('1', '1352324091', '0', '0', 'TrinityCore rev. 2012-11-06 00:53:56 +0400 (1d5b2912d82e+) (Win32, Release)');
INSERT INTO `uptime` VALUES ('1', '1352370345', '0', '0', 'TrinityCore rev. 2012-11-06 00:53:56 +0400 (1d5b2912d82e+) (Win32, Release)');
INSERT INTO `uptime` VALUES ('1', '1352395724', '2408', '1', 'TrinityCore rev. 2012-11-06 00:53:56 +0400 (1d5b2912d82e+) (Win32, Release)');
INSERT INTO `uptime` VALUES ('1', '1352398766', '0', '0', 'TrinityCore rev. 2012-11-06 00:53:56 +0400 (1d5b2912d82e+) (Win32, Release)');
INSERT INTO `uptime` VALUES ('1', '1352483757', '612', '1', 'TrinityCore rev. 2012-11-09 21:49:07 +0400 (81075fe9bd18) (Win32, Release)');
INSERT INTO `uptime` VALUES ('1', '1352484464', '0', '0', 'TrinityCore rev. 2012-11-09 21:49:07 +0400 (81075fe9bd18) (Win32, Release)');
INSERT INTO `uptime` VALUES ('1', '1352485944', '0', '0', 'TrinityCore rev. 2012-11-09 21:49:07 +0400 (81075fe9bd18) (Win32, Release)');
INSERT INTO `uptime` VALUES ('1', '1352486339', '0', '0', 'TrinityCore rev. 2012-11-09 21:49:07 +0400 (81075fe9bd18) (Win32, Release)');
INSERT INTO `uptime` VALUES ('1', '1352486601', '601', '1', 'TrinityCore rev. 2012-11-09 21:49:07 +0400 (81075fe9bd18) (Win32, Release)');
INSERT INTO `uptime` VALUES ('1', '1352487420', '0', '0', 'TrinityCore rev. 2012-11-09 21:49:07 +0400 (81075fe9bd18) (Win32, Release)');
INSERT INTO `uptime` VALUES ('1', '1352487751', '12000', '1', 'TrinityCore rev. 2012-11-09 21:49:07 +0400 (81075fe9bd18) (Win32, Release)');
INSERT INTO `uptime` VALUES ('1', '1352504267', '24609', '1', 'TrinityCore rev. 2012-11-10 02:37:13 +0400 (56fcd3a91bee) (Win32, Release)');
INSERT INTO `uptime` VALUES ('1', '1352541844', '2407', '1', 'TrinityCore rev. 2012-11-10 02:37:13 +0400 (56fcd3a91bee) (Win32, Release)');
INSERT INTO `uptime` VALUES ('1', '1352547533', '3011', '1', 'TrinityCore rev. 2012-11-10 14:58:20 +0400 (66272aaf3da9) (Win32, Release)');
INSERT INTO `uptime` VALUES ('1', '1352552100', '2406', '1', 'TrinityCore rev. 2012-11-10 14:58:20 +0400 (66272aaf3da9) (Win32, Release)');
INSERT INTO `uptime` VALUES ('1', '1352554858', '605', '1', 'TrinityCore rev. 2012-11-10 14:58:20 +0400 (66272aaf3da9) (Win32, Release)');
INSERT INTO `uptime` VALUES ('1', '1352556373', '0', '0', 'TrinityCore rev. 2012-11-10 17:57:47 +0400 (c609713fbd71+) (Win32, Release)');
INSERT INTO `uptime` VALUES ('1', '1352556481', '16800', '1', 'TrinityCore rev. 2012-11-10 17:57:47 +0400 (c609713fbd71+) (Win32, Release)');
INSERT INTO `uptime` VALUES ('1', '1352611860', '0', '0', 'TrinityCore rev. 2012-11-10 23:47:54 +0400 (450ad1ac8f8a) (Win32, Release)');
INSERT INTO `uptime` VALUES ('1', '1352612313', '12600', '1', 'TrinityCore rev. 2012-11-10 23:47:54 +0400 (450ad1ac8f8a) (Win32, Release)');
INSERT INTO `uptime` VALUES ('1', '1352625162', '612', '1', 'TrinityCore rev. 2012-11-10 23:47:54 +0400 (450ad1ac8f8a) (Win32, Release)');
INSERT INTO `uptime` VALUES ('1', '1352628771', '0', '0', 'TrinityCore rev. 2012-11-11 13:58:20 +0400 (081ec07c44a4) (Win32, Release)');
INSERT INTO `uptime` VALUES ('1', '1352629376', '12001', '1', 'TrinityCore rev. 2012-11-11 13:58:20 +0400 (081ec07c44a4) (Win32, Release)');
INSERT INTO `uptime` VALUES ('1', '1352641513', '0', '0', 'TrinityCore rev. 2012-11-11 13:58:20 +0400 (081ec07c44a4) (Win32, Release)');
INSERT INTO `uptime` VALUES ('1', '1352642234', '0', '0', 'TrinityCore rev. 2012-11-11 13:58:20 +0400 (081ec07c44a4) (Win32, Release)');
INSERT INTO `uptime` VALUES ('1', '1352642919', '0', '0', 'TrinityCore rev. 2012-11-11 13:58:20 +0400 (081ec07c44a4) (Win32, Release)');
INSERT INTO `uptime` VALUES ('1', '1352654015', '0', '0', 'TrinityCore rev. 2012-11-11 13:58:20 +0400 (081ec07c44a4) (Win32, Release)');
INSERT INTO `uptime` VALUES ('1', '1352664441', '34207', '1', 'TrinityCore rev. 2012-11-11 22:36:56 +0400 (464b788b591c+) (Win32, Release)');
INSERT INTO `uptime` VALUES ('1', '1352726675', '0', '0', 'TrinityCore rev. 2012-11-11 22:36:56 +0400 (464b788b591c+) (Win32, Release)');
INSERT INTO `uptime` VALUES ('1', '1352737542', '0', '0', 'TrinityCore rev. 2012-11-11 22:36:56 +0400 (464b788b591c+) (Win32, Release)');
INSERT INTO `uptime` VALUES ('1', '1352897482', '0', '0', 'TrinityCore rev. 2012-11-11 22:36:56 +0400 (464b788b591c+) (Win32, Release)');
INSERT INTO `uptime` VALUES ('1', '1352897870', '0', '0', 'TrinityCore rev. 2012-11-11 22:36:56 +0400 (464b788b591c+) (Win32, Release)');
INSERT INTO `uptime` VALUES ('1', '1353056289', '610', '1', 'TrinityCore rev. 2012-11-16 10:39:12 +0400 (49355d59ab85+) (Win32, Release)');
INSERT INTO `uptime` VALUES ('1', '1353057324', '0', '0', 'TrinityCore rev. 2012-11-16 10:39:12 +0400 (49355d59ab85+) (Win32, Release)');
INSERT INTO `uptime` VALUES ('1', '1353087496', '0', '0', 'TrinityCore rev. 0000-00-00 00:00:00 +0000 (Archived) (Win32, Release)');
INSERT INTO `uptime` VALUES ('1', '1353087817', '601', '0', 'TrinityCore rev. 0000-00-00 00:00:00 +0000 (Archived) (Win32, Release)');
INSERT INTO `uptime` VALUES ('1', '1353177218', '0', '0', 'TrinityCore rev. 2012-11-17 22:11:24 +0400 (373459958dde) (Win32, Release)');
INSERT INTO `uptime` VALUES ('1', '1353177666', '0', '0', 'TrinityCore rev. 2012-11-17 22:11:24 +0400 (373459958dde) (Win32, Release)');
INSERT INTO `uptime` VALUES ('1', '1353237295', '609', '1', 'TrinityCore rev. 2012-11-17 22:46:24 +0400 (3cdf049c6377) (Win32, Release)');
INSERT INTO `uptime` VALUES ('1', '1353238027', '601', '1', 'TrinityCore rev. 2012-11-17 22:46:24 +0400 (3cdf049c6377) (Win32, Release)');
INSERT INTO `uptime` VALUES ('1', '1353239244', '0', '0', 'TrinityCore rev. 2012-11-17 22:46:24 +0400 (3cdf049c6377) (Win32, Release)');
INSERT INTO `uptime` VALUES ('1', '1353271566', '0', '0', 'TrinityCore rev. 2012-11-18 23:31:04 +0400 (2072e5924f40) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1353271889', '0', '0', 'TrinityCore rev. 2012-11-18 23:31:04 +0400 (2072e5924f40) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1353328162', '0', '0', 'TrinityCore rev. 2012-11-18 23:31:04 +0400 (2072e5924f40) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1353330033', '0', '0', 'TrinityCore rev. 2012-11-18 23:31:04 +0400 (2072e5924f40) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1353389847', '0', '0', 'TrinityCore rev. 2012-11-18 23:31:04 +0400 (2072e5924f40) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1353681649', '0', '0', 'TrinityCore rev. 2012-11-18 23:31:04 +0400 (2072e5924f40) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1353681798', '0', '0', 'TrinityCore rev. 2012-11-18 23:31:04 +0400 (2072e5924f40) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1353681821', '0', '0', 'TrinityCore rev. 2012-11-18 23:31:04 +0400 (2072e5924f40) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1353779367', '0', '0', 'TrinityCore rev. 2012-11-18 23:31:04 +0400 (2072e5924f40+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1353791997', '31209', '1', 'TrinityCore rev. 2012-11-18 23:31:04 +0400 (2072e5924f40+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1353828962', '0', '0', 'TrinityCore rev. 2012-11-18 23:31:04 +0400 (2072e5924f40+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1353829202', '0', '0', 'TrinityCore rev. 2012-11-18 23:31:04 +0400 (2072e5924f40+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1353833538', '0', '0', 'TrinityCore rev. 2012-11-18 23:31:04 +0400 (2072e5924f40+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1353834856', '0', '0', 'TrinityCore rev. 2012-11-18 23:31:04 +0400 (2072e5924f40+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1353861745', '0', '0', 'TrinityCore rev. 2012-11-18 23:31:04 +0400 (2072e5924f40+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1353861849', '0', '0', 'TrinityCore rev. 2012-11-18 23:31:04 +0400 (2072e5924f40+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1353862209', '0', '0', 'TrinityCore rev. 2012-11-18 23:31:04 +0400 (2072e5924f40+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1353862477', '0', '0', 'TrinityCore rev. 2012-11-18 23:31:04 +0400 (2072e5924f40+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1353881995', '0', '0', 'TrinityCore rev. 2012-11-26 00:54:57 +0400 (89da51dd54fc+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1353882109', '601', '1', 'TrinityCore rev. 2012-11-26 00:54:57 +0400 (89da51dd54fc+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1353882853', '0', '0', 'TrinityCore rev. 2012-11-26 00:54:57 +0400 (89da51dd54fc+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1353957887', '0', '0', 'TrinityCore rev. 2012-11-26 22:54:48 +0400 (400758a0d2d3+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1353958259', '0', '0', 'TrinityCore rev. 2012-11-26 22:54:48 +0400 (400758a0d2d3+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1353958565', '0', '0', 'TrinityCore rev. 2012-11-26 22:54:48 +0400 (400758a0d2d3+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1353958745', '61800', '0', 'TrinityCore rev. 2012-11-26 22:54:48 +0400 (400758a0d2d3+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354034341', '615', '1', 'TrinityCore rev. 2012-11-27 18:25:10 +0400 (6d6dcd3bbdf8+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354035939', '0', '0', 'TrinityCore rev. 2012-11-27 18:25:10 +0400 (6d6dcd3bbdf8+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354196440', '0', '0', 'TrinityCore rev. 2012-11-27 18:25:10 +0400 (6d6dcd3bbdf8+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354197278', '0', '0', 'TrinityCore rev. 2012-11-27 18:25:10 +0400 (6d6dcd3bbdf8+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354197892', '601', '1', 'TrinityCore rev. 2012-11-27 18:25:10 +0400 (6d6dcd3bbdf8+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354199215', '0', '0', 'TrinityCore rev. 2012-11-27 18:25:10 +0400 (6d6dcd3bbdf8+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354199498', '601', '1', 'TrinityCore rev. 2012-11-27 18:25:10 +0400 (6d6dcd3bbdf8+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354200320', '0', '0', 'TrinityCore rev. 2012-11-27 18:25:10 +0400 (6d6dcd3bbdf8+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354200538', '601', '1', 'TrinityCore rev. 2012-11-27 18:25:10 +0400 (6d6dcd3bbdf8+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354201365', '1201', '1', 'TrinityCore rev. 2012-11-27 18:25:10 +0400 (6d6dcd3bbdf8+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354202701', '1200', '1', 'TrinityCore rev. 2012-11-27 18:25:10 +0400 (6d6dcd3bbdf8+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354204160', '21000', '1', 'TrinityCore rev. 2012-11-27 18:25:10 +0400 (6d6dcd3bbdf8+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354228873', '0', '0', 'TrinityCore rev. 2012-11-30 02:23:08 +0400 (ad564467e586+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354229317', '1800', '1', 'TrinityCore rev. 2012-11-30 02:23:08 +0400 (ad564467e586+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354308550', '2411', '1', 'TrinityCore rev. 2012-12-01 00:42:21 +0400 (4de2e27cf046+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354312272', '0', '0', 'TrinityCore rev. 2012-12-01 00:42:21 +0400 (4de2e27cf046+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354350117', '0', '0', 'TrinityCore rev. 2012-12-01 00:42:21 +0400 (4de2e27cf046+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354353895', '0', '0', 'TrinityCore rev. 2012-12-01 00:42:21 +0400 (4de2e27cf046+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354389317', '3613', '1', 'TrinityCore rev. 2012-12-01 20:19:32 +0400 (d2ef6530dd14+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354459654', '617', '1', 'TrinityCore rev. 2012-12-01 20:19:32 +0400 (d2ef6530dd14+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354545842', '0', '0', 'TrinityCore rev. 2012-12-02 23:24:32 +0400 (29cc8e21b2b8+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354545969', '0', '0', 'TrinityCore rev. 2012-12-02 23:24:32 +0400 (29cc8e21b2b8+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354546240', '0', '0', 'TrinityCore rev. 2012-12-02 23:24:32 +0400 (29cc8e21b2b8+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354546597', '0', '0', 'TrinityCore rev. 2012-12-02 23:24:32 +0400 (29cc8e21b2b8+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354554016', '613', '1', 'TrinityCore rev. 2012-12-03 19:14:08 +0400 (c4fbf84a53b7+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354555382', '1806', '0', 'TrinityCore rev. 2012-12-03 19:14:08 +0400 (c4fbf84a53b7+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354557290', '3001', '1', 'TrinityCore rev. 2012-12-03 19:14:08 +0400 (c4fbf84a53b7+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354560829', '608', '1', 'TrinityCore rev. 2012-12-03 19:14:08 +0400 (c4fbf84a53b7+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354562018', '0', '0', 'TrinityCore rev. 2012-12-03 19:14:08 +0400 (c4fbf84a53b7+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354566600', '32401', '0', 'TrinityCore rev. 2012-12-06 09:38:39 +0400 (5876d741a84b) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354599601', '0', '0', 'TrinityCore rev. 2012-12-06 09:38:39 +0400 (5876d741a84b) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354646577', '0', '0', 'TrinityCore rev. 2012-12-06 09:38:39 +0400 (5876d741a84b) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354646638', '0', '0', 'TrinityCore rev. 2012-12-06 09:38:39 +0400 (5876d741a84b) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354647365', '0', '0', 'TrinityCore rev. 2012-12-06 09:38:39 +0400 (5876d741a84b) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354647878', '0', '0', 'TrinityCore rev. 2012-12-06 09:38:39 +0400 (5876d741a84b) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354647932', '1800', '0', 'TrinityCore rev. 2012-12-06 09:38:39 +0400 (5876d741a84b) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354654480', '0', '0', 'TrinityCore rev. 2012-12-06 09:38:39 +0400 (5876d741a84b) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354654519', '0', '0', 'TrinityCore rev. 2012-12-06 09:38:39 +0400 (5876d741a84b) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354654717', '33001', '0', 'TrinityCore rev. 2012-12-06 09:38:39 +0400 (5876d741a84b) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354689336', '0', '0', 'TrinityCore rev. 2012-12-06 09:38:39 +0400 (5876d741a84b) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354689944', '0', '0', 'TrinityCore rev. 2012-12-06 09:38:39 +0400 (5876d741a84b) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354690384', '0', '0', 'TrinityCore rev. 2012-12-06 09:38:39 +0400 (5876d741a84b) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354690718', '0', '0', 'TrinityCore rev. 2012-12-06 09:38:39 +0400 (5876d741a84b) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354690840', '0', '0', 'TrinityCore rev. 2012-12-06 09:38:39 +0400 (5876d741a84b) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354692642', '0', '0', 'TrinityCore rev. 2012-12-06 09:38:39 +0400 (5876d741a84b) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354692864', '0', '0', 'TrinityCore rev. 2012-12-06 09:38:39 +0400 (5876d741a84b) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354693055', '1800', '1', 'TrinityCore rev. 2012-12-06 09:38:39 +0400 (5876d741a84b) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354719837', '0', '0', 'TrinityCore rev. 2012-12-06 09:38:39 +0400 (5876d741a84b) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354721557', '600', '0', 'TrinityCore rev. 2012-12-06 09:38:39 +0400 (5876d741a84b) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354722751', '0', '0', 'TrinityCore rev. 2012-12-06 09:38:39 +0400 (5876d741a84b) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354722883', '0', '0', 'TrinityCore rev. 2012-12-06 09:38:39 +0400 (5876d741a84b) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354723591', '0', '0', 'TrinityCore rev. 2012-12-06 09:38:39 +0400 (5876d741a84b) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354724073', '0', '0', 'TrinityCore rev. 2012-12-06 09:38:39 +0400 (5876d741a84b) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354738357', '0', '0', 'TrinityCore rev. 2012-12-06 09:38:39 +0400 (5876d741a84b) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354739118', '0', '0', 'TrinityCore rev. 2012-12-06 09:38:39 +0400 (5876d741a84b) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354740115', '34800', '0', 'TrinityCore rev. 2012-12-06 09:38:39 +0400 (5876d741a84b) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354745273', '1213', '1', 'TrinityCore rev. 2012-12-06 00:23:27 +0400 (5901d98b13c0+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354747121', '0', '0', 'TrinityCore rev. 2012-12-06 00:23:27 +0400 (5901d98b13c0+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354747418', '0', '0', 'TrinityCore rev. 2012-12-06 00:23:27 +0400 (5901d98b13c0+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354747710', '1200', '1', 'TrinityCore rev. 2012-12-06 00:23:27 +0400 (5901d98b13c0+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354749396', '600', '1', 'TrinityCore rev. 2012-12-06 00:23:27 +0400 (5901d98b13c0+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354750409', '0', '0', 'TrinityCore rev. 2012-12-06 00:23:27 +0400 (5901d98b13c0+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354750814', '21000', '1', 'TrinityCore rev. 2012-12-06 00:23:27 +0400 (5901d98b13c0+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354772009', '0', '0', 'TrinityCore rev. 2012-12-06 00:23:27 +0400 (5901d98b13c0+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354796339', '1217', '1', 'TrinityCore rev. 2012-12-06 09:38:39 +0400 (5876d741a84b) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354797643', '1207', '1', 'TrinityCore rev. 2012-12-06 09:38:39 +0400 (5876d741a84b) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354799417', '0', '0', 'TrinityCore rev. 2012-12-06 09:38:39 +0400 (5876d741a84b) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354799656', '0', '0', 'TrinityCore rev. 2012-12-06 16:13:57 +0400 (108667f6d0b1) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354800117', '600', '0', 'TrinityCore rev. 2012-12-06 16:13:57 +0400 (108667f6d0b1) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354800666', '0', '0', 'TrinityCore rev. 2012-12-06 09:38:39 +0400 (5876d741a84b) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354800994', '0', '0', 'TrinityCore rev. 2012-12-06 09:38:39 +0400 (5876d741a84b) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354801241', '0', '0', 'TrinityCore rev. 2012-12-06 09:38:39 +0400 (5876d741a84b) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354803686', '0', '0', 'TrinityCore rev. 2012-12-06 18:13:54 +0400 (42b9fe7ce60d) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354803738', '1200', '0', 'TrinityCore rev. 2012-12-06 18:13:54 +0400 (42b9fe7ce60d) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354810940', '1201', '1', 'TrinityCore rev. 2012-12-06 09:38:39 +0400 (5876d741a84b) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354812677', '624', '1', 'TrinityCore rev. 2012-12-06 09:38:39 +0400 (5876d741a84b) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354813995', '0', '0', 'TrinityCore rev. 2012-12-06 09:38:39 +0400 (5876d741a84b) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354814143', '1201', '1', 'TrinityCore rev. 2012-12-06 09:38:39 +0400 (5876d741a84b) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354815740', '0', '0', 'TrinityCore rev. 2012-12-06 09:38:39 +0400 (5876d741a84b) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354815997', '0', '0', 'TrinityCore rev. 2012-12-06 09:38:39 +0400 (5876d741a84b) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354816117', '601', '1', 'TrinityCore rev. 2012-12-06 09:38:39 +0400 (5876d741a84b) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354826451', '2409', '0', 'TrinityCore rev. 2012-12-06 18:13:54 +0400 (42b9fe7ce60d) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354830792', '0', '0', 'TrinityCore rev. 2012-12-06 18:13:54 +0400 (42b9fe7ce60d) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354830986', '0', '0', 'TrinityCore rev. 2012-12-06 18:13:54 +0400 (42b9fe7ce60d) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354831266', '0', '0', 'TrinityCore rev. 2012-12-06 18:13:54 +0400 (42b9fe7ce60d) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354831785', '0', '0', 'TrinityCore rev. 2012-12-06 18:13:54 +0400 (42b9fe7ce60d) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354831941', '0', '0', 'TrinityCore rev. 2012-12-06 18:13:54 +0400 (42b9fe7ce60d) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354832048', '0', '0', 'TrinityCore rev. 2012-12-06 18:13:54 +0400 (42b9fe7ce60d) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354884615', '0', '0', 'TrinityCore rev. 2012-12-06 18:13:54 +0400 (42b9fe7ce60d) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354884675', '0', '0', 'TrinityCore rev. 2012-12-06 18:13:54 +0400 (42b9fe7ce60d) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354885102', '0', '0', 'TrinityCore rev. 2012-12-06 18:13:54 +0400 (42b9fe7ce60d) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354885586', '0', '0', 'TrinityCore rev. 2012-12-06 18:13:54 +0400 (42b9fe7ce60d) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354903635', '0', '0', 'TrinityCore rev. 2012-12-06 18:13:54 +0400 (42b9fe7ce60d) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354906605', '1206', '1', 'TrinityCore rev. 2012-12-06 18:13:54 +0400 (42b9fe7ce60d) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354908117', '58802', '1', 'TrinityCore rev. 2012-12-06 18:13:54 +0400 (42b9fe7ce60d) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354975864', '0', '0', 'TrinityCore rev. 2012-12-06 18:13:54 +0400 (42b9fe7ce60d) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354976146', '0', '0', 'TrinityCore rev. 2012-12-06 18:13:54 +0400 (42b9fe7ce60d) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354976665', '601', '1', 'TrinityCore rev. 2012-12-06 18:13:54 +0400 (42b9fe7ce60d) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354979222', '0', '0', 'TrinityCore rev. 2012-12-06 18:13:54 +0400 (42b9fe7ce60d) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354979459', '0', '0', 'TrinityCore rev. 2012-12-06 18:13:54 +0400 (42b9fe7ce60d) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354995803', '0', '0', 'TrinityCore rev. 2012-12-06 18:13:54 +0400 (42b9fe7ce60d) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354995949', '0', '0', 'TrinityCore rev. 2012-12-06 18:13:54 +0400 (42b9fe7ce60d) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354996371', '0', '0', 'TrinityCore rev. 2012-12-06 18:13:54 +0400 (42b9fe7ce60d) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354997041', '0', '0', 'TrinityCore rev. 2012-12-06 18:13:54 +0400 (42b9fe7ce60d) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354997648', '0', '0', 'TrinityCore rev. 2012-12-06 18:13:54 +0400 (42b9fe7ce60d) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1354998194', '0', '0', 'TrinityCore rev. 2012-12-06 18:13:54 +0400 (42b9fe7ce60d) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1355060956', '0', '0', 'TrinityCore rev. 2012-12-06 18:13:54 +0400 (42b9fe7ce60d) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1355075670', '0', '0', 'TrinityCore rev. 2012-12-09 19:31:59 +0400 (db330386dab7) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1355075827', '1200', '1', 'TrinityCore rev. 2012-12-09 19:31:59 +0400 (db330386dab7) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1355077629', '0', '0', 'TrinityCore rev. 2012-12-09 19:31:59 +0400 (db330386dab7) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1355078144', '1200', '1', 'TrinityCore rev. 2012-12-09 19:31:59 +0400 (db330386dab7) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1355154800', '0', '0', 'TrinityCore rev. 2012-12-09 19:31:59 +0400 (db330386dab7+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1355155212', '0', '0', 'TrinityCore rev. 2012-12-09 19:31:59 +0400 (db330386dab7+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1355155519', '600', '0', 'TrinityCore rev. 2012-12-09 19:31:59 +0400 (db330386dab7+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1355163974', '604', '1', 'TrinityCore rev. 2012-12-09 19:31:59 +0400 (db330386dab7+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1355164632', '0', '0', 'TrinityCore rev. 2012-12-09 19:31:59 +0400 (db330386dab7+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1355165249', '2401', '1', 'TrinityCore rev. 2012-12-09 19:31:59 +0400 (db330386dab7+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1355241732', '3018', '1', 'TrinityCore rev. 2012-12-09 19:31:59 +0400 (db330386dab7+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1355245149', '0', '0', 'TrinityCore rev. 2012-12-09 19:31:59 +0400 (db330386dab7+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1355245281', '0', '0', 'TrinityCore rev. 2012-12-09 19:31:59 +0400 (db330386dab7+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1355248978', '0', '0', 'TrinityCore rev. 2012-12-09 19:31:59 +0400 (db330386dab7+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1355514423', '0', '0', 'TrinityCore rev. 2012-12-09 19:31:59 +0400 (db330386dab7+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1355514474', '0', '0', 'TrinityCore rev. 2012-12-09 19:31:59 +0400 (db330386dab7+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1355522312', '0', '0', 'TrinityCore rev. 2012-12-15 01:36:15 +0400 (c68697e18e02+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1355522808', '1200', '1', 'TrinityCore rev. 2012-12-15 01:36:15 +0400 (c68697e18e02+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1355568605', '0', '0', 'TrinityCore rev. 2012-12-15 01:36:15 +0400 (c68697e18e02+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1355568886', '0', '0', 'TrinityCore rev. 2012-12-15 01:36:15 +0400 (c68697e18e02+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1355578481', '609', '1', 'TrinityCore rev. 2012-12-15 01:36:15 +0400 (c68697e18e02+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1355579366', '1801', '1', 'TrinityCore rev. 2012-12-15 01:36:15 +0400 (c68697e18e02+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1355581415', '601', '1', 'TrinityCore rev. 2012-12-15 01:36:15 +0400 (c68697e18e02+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1355590562', '0', '0', 'TrinityCore rev. 2012-12-15 01:36:15 +0400 (c68697e18e02+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1355599264', '0', '0', 'TrinityCore rev. 2012-12-15 11:55:49 +0400 (76a8eb66cfe3+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1355599467', '0', '0', 'TrinityCore rev. 2012-12-15 11:55:49 +0400 (76a8eb66cfe3+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1355648577', '0', '0', 'TrinityCore rev. 2012-12-15 11:55:49 +0400 (76a8eb66cfe3+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1355649157', '6001', '1', 'TrinityCore rev. 2012-12-15 11:55:49 +0400 (76a8eb66cfe3+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1355655300', '0', '0', 'TrinityCore rev. 2012-12-15 11:55:49 +0400 (76a8eb66cfe3+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1355656587', '0', '0', 'TrinityCore rev. 2012-12-15 11:55:49 +0400 (76a8eb66cfe3+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1355656641', '0', '0', 'TrinityCore rev. 2012-12-15 11:55:49 +0400 (76a8eb66cfe3+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1355657020', '0', '0', 'TrinityCore rev. 2012-12-15 11:55:49 +0400 (76a8eb66cfe3+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1355657202', '0', '0', 'TrinityCore rev. 2012-12-15 11:55:49 +0400 (76a8eb66cfe3+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1355657539', '0', '0', 'TrinityCore rev. 2012-12-15 11:55:49 +0400 (76a8eb66cfe3+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1355657618', '0', '0', 'TrinityCore rev. 2012-12-15 11:55:49 +0400 (76a8eb66cfe3+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1355657853', '0', '0', 'TrinityCore rev. 2012-12-15 11:55:49 +0400 (76a8eb66cfe3+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1355664224', '6001', '1', 'TrinityCore rev. 2012-12-15 11:55:49 +0400 (76a8eb66cfe3+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1355671281', '0', '0', 'TrinityCore rev. 2012-12-15 11:55:49 +0400 (76a8eb66cfe3+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1355671532', '0', '0', 'TrinityCore rev. 2012-12-15 11:55:49 +0400 (76a8eb66cfe3+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1355672317', '0', '0', 'TrinityCore rev. 2012-12-15 11:55:49 +0400 (76a8eb66cfe3+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1355690133', '0', '0', 'TrinityCore rev. 2012-12-16 23:34:11 +0400 (9039b76b102e+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1355690338', '0', '0', 'TrinityCore rev. 2012-12-16 23:34:11 +0400 (9039b76b102e+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1355691949', '2401', '0', 'TrinityCore rev. 2012-12-16 23:34:11 +0400 (9039b76b102e+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1355748208', '614', '1', 'TrinityCore rev. 2012-12-16 23:34:11 +0400 (9039b76b102e+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1355763489', '0', '0', 'TrinityCore rev. 2012-12-17 20:47:55 +0400 (dfd52132b3d2) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1355764324', '1201', '0', 'TrinityCore rev. 2012-12-17 20:47:55 +0400 (dfd52132b3d2) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1355780241', '0', '0', 'TrinityCore rev. 2012-12-18 00:43:45 +0400 (cedf57b484bf) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1355783586', '612', '0', 'TrinityCore rev. 2012-12-18 00:43:45 +0400 (cedf57b484bf) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1355855754', '1809', '2', 'TrinityCore rev. 2012-12-18 00:43:45 +0400 (cedf57b484bf+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1355857633', '1206', '1', 'TrinityCore rev. 2012-12-18 00:43:45 +0400 (cedf57b484bf+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1355863744', '64231', '2', 'TrinityCore rev. 2012-12-18 00:43:45 +0400 (cedf57b484bf+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1355925780', '0', '0', 'TrinityCore rev. 2012-12-18 00:43:45 +0400 (cedf57b484bf+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1355935078', '0', '0', 'TrinityCore rev. 2012-12-18 00:43:45 +0400 (cedf57b484bf+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1355935519', '0', '0', 'TrinityCore rev. 2012-12-18 00:43:45 +0400 (cedf57b484bf+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1355935769', '0', '0', 'TrinityCore rev. 2012-12-18 00:43:45 +0400 (cedf57b484bf+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1355935847', '0', '0', 'TrinityCore rev. 2012-12-18 00:43:45 +0400 (cedf57b484bf+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1355936067', '0', '0', 'TrinityCore rev. 2012-12-18 00:43:45 +0400 (cedf57b484bf+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1355939958', '0', '0', 'TrinityCore rev. 2012-12-19 21:47:58 +0400 (57e4b3e3f1f0+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1355940440', '0', '0', 'TrinityCore rev. 2012-12-19 21:47:58 +0400 (57e4b3e3f1f0+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1355940706', '0', '0', 'TrinityCore rev. 2012-12-19 21:47:58 +0400 (57e4b3e3f1f0+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1355942850', '0', '0', 'TrinityCore rev. 2012-12-18 00:43:45 +0400 (cedf57b484bf) (Win32, MinSizeRel)');
INSERT INTO `uptime` VALUES ('1', '1355943579', '0', '0', 'TrinityCore rev. 2012-12-18 00:43:45 +0400 (cedf57b484bf) (Win32, MinSizeRel)');
INSERT INTO `uptime` VALUES ('1', '1355944905', '1207', '0', 'TrinityCore rev. 2012-12-19 21:47:58 +0400 (57e4b3e3f1f0+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1355949602', '0', '0', 'TrinityCore rev. 2012-12-19 21:47:58 +0400 (57e4b3e3f1f0+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1355950165', '601', '1', 'TrinityCore rev. 2012-12-19 21:47:58 +0400 (57e4b3e3f1f0+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1355954320', '0', '0', 'TrinityCore rev. 2012-12-18 00:43:45 +0400 (cedf57b484bf) (Win32, MinSizeRel)');
INSERT INTO `uptime` VALUES ('1', '1355954505', '0', '0', 'TrinityCore rev. 2012-12-18 00:43:45 +0400 (cedf57b484bf) (Win32, MinSizeRel)');
INSERT INTO `uptime` VALUES ('1', '1355954974', '0', '0', 'TrinityCore rev. 2012-12-18 00:43:45 +0400 (cedf57b484bf) (Win32, MinSizeRel)');
INSERT INTO `uptime` VALUES ('1', '1355956343', '0', '0', 'TrinityCore rev. 2012-12-18 00:43:45 +0400 (cedf57b484bf) (Win32, MinSizeRel)');
INSERT INTO `uptime` VALUES ('1', '1355956800', '0', '0', 'TrinityCore rev. 2012-12-18 00:43:45 +0400 (cedf57b484bf) (Win32, MinSizeRel)');
INSERT INTO `uptime` VALUES ('1', '1355956878', '0', '0', 'TrinityCore rev. 2012-12-18 00:43:45 +0400 (cedf57b484bf) (Win32, MinSizeRel)');
INSERT INTO `uptime` VALUES ('1', '1355957065', '0', '0', 'TrinityCore rev. 2012-12-18 00:43:45 +0400 (cedf57b484bf) (Win32, MinSizeRel)');
INSERT INTO `uptime` VALUES ('1', '1355957161', '0', '0', 'TrinityCore rev. 2012-12-18 00:43:45 +0400 (cedf57b484bf) (Win32, MinSizeRel)');
INSERT INTO `uptime` VALUES ('1', '1355957294', '0', '0', 'TrinityCore rev. 2012-12-18 00:43:45 +0400 (cedf57b484bf) (Win32, MinSizeRel)');
INSERT INTO `uptime` VALUES ('1', '1355957738', '0', '0', 'TrinityCore rev. 2012-12-18 00:43:45 +0400 (cedf57b484bf) (Win32, MinSizeRel)');
INSERT INTO `uptime` VALUES ('1', '1355957999', '0', '0', 'TrinityCore rev. 2012-12-18 00:43:45 +0400 (cedf57b484bf) (Win32, MinSizeRel)');
INSERT INTO `uptime` VALUES ('1', '1355958390', '0', '0', 'TrinityCore rev. 2012-12-18 00:43:45 +0400 (cedf57b484bf) (Win32, MinSizeRel)');
INSERT INTO `uptime` VALUES ('1', '1355958546', '0', '0', 'TrinityCore rev. 2012-12-18 00:43:45 +0400 (cedf57b484bf) (Win32, MinSizeRel)');
INSERT INTO `uptime` VALUES ('1', '1355958725', '0', '0', 'TrinityCore rev. 2012-12-18 00:43:45 +0400 (cedf57b484bf) (Win32, MinSizeRel)');
INSERT INTO `uptime` VALUES ('1', '1356011933', '0', '0', 'TrinityCore rev. 2012-12-18 00:43:45 +0400 (cedf57b484bf) (Win32, MinSizeRel)');
INSERT INTO `uptime` VALUES ('1', '1356012135', '0', '0', 'TrinityCore rev. 2012-12-18 00:43:45 +0400 (cedf57b484bf) (Win32, MinSizeRel)');
INSERT INTO `uptime` VALUES ('1', '1356012916', '0', '0', 'TrinityCore rev. 2012-12-18 00:43:45 +0400 (cedf57b484bf) (Win32, MinSizeRel)');
INSERT INTO `uptime` VALUES ('1', '1356013243', '0', '0', 'TrinityCore rev. 2012-12-18 00:43:45 +0400 (cedf57b484bf) (Win32, MinSizeRel)');
INSERT INTO `uptime` VALUES ('1', '1356013948', '0', '0', 'TrinityCore rev. 2012-12-18 00:43:45 +0400 (cedf57b484bf) (Win32, MinSizeRel)');
INSERT INTO `uptime` VALUES ('1', '1356015798', '12604', '2', 'TrinityCore rev. 2012-12-19 21:47:58 +0400 (57e4b3e3f1f0+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1356028537', '2407', '2', 'TrinityCore rev. 2012-12-19 21:47:58 +0400 (57e4b3e3f1f0+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1356031434', '7801', '2', 'TrinityCore rev. 2012-12-19 21:47:58 +0400 (57e4b3e3f1f0+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1356039340', '600', '1', 'TrinityCore rev. 2012-12-19 21:47:58 +0400 (57e4b3e3f1f0+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1356040182', '0', '0', 'TrinityCore rev. 2012-12-19 21:47:58 +0400 (57e4b3e3f1f0+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1356040804', '0', '0', 'TrinityCore rev. 2012-12-19 21:47:58 +0400 (57e4b3e3f1f0+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1356040934', '5400', '2', 'TrinityCore rev. 2012-12-19 21:47:58 +0400 (57e4b3e3f1f0+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1356046167', '0', '0', 'TrinityCore rev. 2012-12-20 02:03:53 +0400 (e0a4ea4062e9+) (Win32, MinSizeRel)');
INSERT INTO `uptime` VALUES ('1', '1356046544', '0', '0', 'TrinityCore rev. 2012-12-20 02:03:53 +0400 (e0a4ea4062e9+) (Win32, MinSizeRel)');
INSERT INTO `uptime` VALUES ('1', '1356046669', '47409', '2', 'TrinityCore rev. 2012-12-19 21:47:58 +0400 (57e4b3e3f1f0+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1356100659', '15001', '3', 'TrinityCore rev. 2012-12-19 21:47:58 +0400 (57e4b3e3f1f0+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1356115922', '1805', '2', 'TrinityCore rev. 2012-12-19 21:47:58 +0400 (57e4b3e3f1f0+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1356118335', '3615', '2', 'TrinityCore rev. 2012-12-20 02:03:53 +0400 (e0a4ea4062e9+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1356122774', '0', '0', 'TrinityCore rev. 2012-12-20 02:03:53 +0400 (e0a4ea4062e9+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1356123217', '0', '0', 'TrinityCore rev. 2012-12-20 02:03:53 +0400 (e0a4ea4062e9+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1356123286', '600', '1', 'TrinityCore rev. 2012-12-20 02:03:53 +0400 (e0a4ea4062e9+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1356124454', '0', '0', 'TrinityCore rev. 2012-12-20 02:03:53 +0400 (e0a4ea4062e9+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1356125215', '70800', '2', 'TrinityCore rev. 2012-12-20 02:03:53 +0400 (e0a4ea4062e9+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1356196331', '1211', '1', 'TrinityCore rev. 2012-12-20 02:03:53 +0400 (e0a4ea4062e9+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1356197903', '62401', '0', 'TrinityCore rev. 2012-12-20 02:03:53 +0400 (e0a4ea4062e9+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1356198760', '0', '0', 'TrinityCore rev. 2012-12-20 02:03:53 +0400 (e0a4ea4062e9+) (Win32, MinSizeRel)');
INSERT INTO `uptime` VALUES ('1', '1356260868', '0', '0', 'TrinityCore rev. 2012-12-20 02:03:53 +0400 (e0a4ea4062e9+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1356260957', '0', '0', 'TrinityCore rev. 2012-12-20 02:03:53 +0400 (e0a4ea4062e9+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1356261261', '0', '0', 'TrinityCore rev. 2012-12-20 02:03:53 +0400 (e0a4ea4062e9+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1356261651', '0', '0', 'TrinityCore rev. 2012-12-20 02:03:53 +0400 (e0a4ea4062e9+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1356261821', '0', '0', 'TrinityCore rev. 2012-12-20 02:03:53 +0400 (e0a4ea4062e9+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1356262346', '601', '1', 'TrinityCore rev. 2012-12-20 02:03:53 +0400 (e0a4ea4062e9+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1356263135', '0', '0', 'TrinityCore rev. 2012-12-20 02:03:53 +0400 (e0a4ea4062e9+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1356263577', '13210', '3', 'TrinityCore rev. 2012-12-20 02:03:53 +0400 (e0a4ea4062e9+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1356277012', '0', '0', 'TrinityCore rev. 2012-12-20 02:03:53 +0400 (e0a4ea4062e9+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1356277479', '4809', '2', 'TrinityCore rev. 2012-12-22 17:01:55 +0400 (3bffdbde5eb6+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1356282924', '13803', '2', 'TrinityCore rev. 2012-12-22 17:01:55 +0400 (3bffdbde5eb6+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1356334494', '7806', '0', 'TrinityCore rev. 2012-12-22 17:01:55 +0400 (3bffdbde5eb6+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1356359399', '15007', '0', 'TrinityCore rev. 2012-12-22 17:01:55 +0400 (3bffdbde5eb6+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1356374834', '604', '1', 'TrinityCore rev. 2012-12-22 17:01:55 +0400 (3bffdbde5eb6+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1356376221', '601', '1', 'TrinityCore rev. 2012-12-22 17:01:55 +0400 (3bffdbde5eb6+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1356377663', '0', '0', 'TrinityCore rev. 2012-12-22 17:01:55 +0400 (3bffdbde5eb6+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1356380101', '0', '0', 'TrinityCore rev. 2012-12-22 17:01:55 +0400 (3bffdbde5eb6+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1356381294', '0', '0', 'TrinityCore rev. 2012-12-22 17:01:55 +0400 (3bffdbde5eb6+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1356381881', '0', '0', 'TrinityCore rev. 2012-12-22 17:01:55 +0400 (3bffdbde5eb6+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1356385957', '601', '1', 'TrinityCore rev. 2012-12-22 17:01:55 +0400 (3bffdbde5eb6+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1356386670', '2400', '1', 'TrinityCore rev. 2012-12-22 17:01:55 +0400 (3bffdbde5eb6+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1356413765', '15601', '0', 'TrinityCore rev. 2012-12-22 17:01:55 +0400 (3bffdbde5eb6+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1356447453', '0', '0', 'TrinityCore rev. 2012-12-22 17:01:55 +0400 (3bffdbde5eb6+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1356450438', '0', '0', 'TrinityCore rev. 2012-12-22 17:01:55 +0400 (3bffdbde5eb6+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1356452620', '0', '0', 'TrinityCore rev. 2012-12-22 17:01:55 +0400 (3bffdbde5eb6+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1356453080', '0', '0', 'TrinityCore rev. 2012-12-22 17:01:55 +0400 (3bffdbde5eb6+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1356457626', '0', '0', 'TrinityCore rev. 2012-12-22 17:01:55 +0400 (3bffdbde5eb6+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1356457669', '1211', '1', 'TrinityCore rev. 2012-12-22 17:01:55 +0400 (3bffdbde5eb6+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1356459630', '0', '0', 'TrinityCore rev. 2012-12-25 20:06:58 +0400 (9a52c5dd5463+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1356532495', '0', '0', 'TrinityCore rev. 2012-12-25 20:06:58 +0400 (9a52c5dd5463+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1356533466', '0', '0', 'TrinityCore rev. 2012-12-25 20:06:58 +0400 (9a52c5dd5463+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1356534429', '0', '0', 'TrinityCore rev. 2012-12-25 20:06:58 +0400 (9a52c5dd5463+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1356535063', '0', '0', 'TrinityCore rev. 2012-12-25 20:06:58 +0400 (9a52c5dd5463+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1356539867', '1812', '2', 'TrinityCore rev. 2012-12-25 20:06:58 +0400 (9a52c5dd5463+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1356542265', '88201', '0', 'TrinityCore rev. 2012-12-25 20:06:58 +0400 (9a52c5dd5463+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1356631654', '0', '0', 'TrinityCore rev. 2012-12-25 20:06:58 +0400 (9a52c5dd5463+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1356726411', '0', '0', 'TrinityCore rev. 2012-12-25 20:06:58 +0400 (9a52c5dd5463+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1356775999', '600', '1', 'TrinityCore rev. 2012-12-25 20:06:58 +0400 (9a52c5dd5463+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1356780946', '600', '1', 'TrinityCore rev. 2012-12-25 20:06:58 +0400 (9a52c5dd5463+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1356781769', '0', '0', 'TrinityCore rev. 2012-12-25 20:06:58 +0400 (9a52c5dd5463+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1356823261', '0', '0', 'TrinityCore rev. 2012-12-30 01:23:20 +0400 (d8d56f25dc5a+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1356824766', '0', '0', 'TrinityCore rev. 2012-12-30 01:23:20 +0400 (d8d56f25dc5a+) (Win32, MinSizeRel)');
INSERT INTO `uptime` VALUES ('1', '1356824835', '601', '1', 'TrinityCore rev. 2012-12-30 01:23:20 +0400 (d8d56f25dc5a+) (Win32, MinSizeRel)');
INSERT INTO `uptime` VALUES ('1', '1356885818', '0', '0', 'TrinityCore rev. 2012-12-30 11:32:47 +0400 (97bc9bba1ee1+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1356946561', '0', '0', 'TrinityCore rev. 2012-12-30 11:32:47 +0400 (97bc9bba1ee1+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1356946923', '0', '0', 'TrinityCore rev. 2012-12-30 11:32:47 +0400 (97bc9bba1ee1+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1356947083', '0', '0', 'TrinityCore rev. 2012-12-30 11:32:47 +0400 (97bc9bba1ee1+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1356947191', '2400', '1', 'TrinityCore rev. 2012-12-30 11:32:47 +0400 (97bc9bba1ee1+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1356962104', '0', '0', 'TrinityCore rev. 2012-12-30 11:32:47 +0400 (97bc9bba1ee1+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1356963889', '9601', '1', 'TrinityCore rev. 2012-12-30 11:32:47 +0400 (97bc9bba1ee1+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1356980463', '3600', '0', 'TrinityCore rev. 2012-12-30 11:32:47 +0400 (97bc9bba1ee1+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1356989659', '601', '1', 'TrinityCore rev. 2012-12-30 11:32:47 +0400 (97bc9bba1ee1+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1356990778', '0', '0', 'TrinityCore rev. 2012-12-30 11:32:47 +0400 (97bc9bba1ee1+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1356991245', '14401', '1', 'TrinityCore rev. 2012-12-30 11:32:47 +0400 (97bc9bba1ee1+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1357027271', '0', '0', 'TrinityCore rev. 2012-12-30 11:32:47 +0400 (97bc9bba1ee1+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1357069606', '0', '0', 'TrinityCore rev. 2012-12-30 11:32:47 +0400 (97bc9bba1ee1+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1357070784', '0', '0', 'TrinityCore rev. 2012-12-30 11:32:47 +0400 (97bc9bba1ee1+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1357071733', '0', '0', 'TrinityCore rev. 2012-12-30 11:32:47 +0400 (97bc9bba1ee1+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1357072257', '0', '0', 'TrinityCore rev. 2012-12-30 11:32:47 +0400 (97bc9bba1ee1+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1357072968', '0', '0', 'TrinityCore rev. 2012-12-30 11:32:47 +0400 (97bc9bba1ee1+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1357073295', '601', '1', 'TrinityCore rev. 2012-12-30 11:32:47 +0400 (97bc9bba1ee1+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1357158415', '0', '0', 'TrinityCore rev. 2013-01-02 00:56:31 +0400 (260bf95b123e+) (Win64, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1357410572', '0', '0', 'TrinityCore rev. 2013-01-04 20:49:40 +0400 (d6c95748c468) (Win64, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1357470529', '1208', '0', 'TrinityCore rev. 2013-01-04 20:49:40 +0400 (d6c95748c468+) (Win64, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1357473278', '0', '0', 'TrinityCore rev. 2013-01-04 20:49:40 +0400 (d6c95748c468+) (Win64, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1357474817', '0', '0', 'TrinityCore rev. 2013-01-04 20:49:40 +0400 (d6c95748c468+) (Win64, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1357474935', '602', '0', 'TrinityCore rev. 2013-01-04 20:49:40 +0400 (d6c95748c468+) (Win64, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1357482891', '0', '0', 'TrinityCore rev. 2013-01-04 20:49:40 +0400 (d6c95748c468+) (Win64, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1357483173', '0', '0', 'TrinityCore rev. 2013-01-04 20:49:40 +0400 (d6c95748c468+) (Win64, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1357483642', '0', '0', 'TrinityCore rev. 2013-01-04 20:49:40 +0400 (d6c95748c468+) (Win64, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1357483775', '1201', '1', 'TrinityCore rev. 2013-01-04 20:49:40 +0400 (d6c95748c468+) (Win64, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1357487350', '0', '0', 'TrinityCore rev. 2013-01-04 20:49:40 +0400 (d6c95748c468+) (Win64, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1357487516', '0', '0', 'TrinityCore rev. 2013-01-04 20:49:40 +0400 (d6c95748c468+) (Win64, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1357488555', '0', '0', 'TrinityCore rev. 2013-01-04 20:49:40 +0400 (d6c95748c468+) (Win64, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1357489221', '0', '0', 'TrinityCore rev. 2013-01-04 20:49:40 +0400 (d6c95748c468+) (Win64, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1357494507', '0', '0', 'TrinityCore rev. 2013-01-04 20:49:40 +0400 (d6c95748c468+) (Win64, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1357494876', '0', '0', 'TrinityCore rev. 2013-01-04 20:49:40 +0400 (d6c95748c468+) (Win64, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1357495464', '0', '0', 'TrinityCore rev. 2013-01-04 20:49:40 +0400 (d6c95748c468+) (Win64, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1357496040', '0', '0', 'TrinityCore rev. 2013-01-04 20:49:40 +0400 (d6c95748c468+) (Win64, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1357496275', '0', '0', 'TrinityCore rev. 2013-01-04 20:49:40 +0400 (d6c95748c468+) (Win64, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1357497162', '601', '0', 'TrinityCore rev. 2013-01-04 20:49:40 +0400 (d6c95748c468+) (Win64, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1357498148', '0', '0', 'TrinityCore rev. 2013-01-04 20:49:40 +0400 (d6c95748c468+) (Win64, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1357498486', '0', '0', 'TrinityCore rev. 2013-01-04 20:49:40 +0400 (d6c95748c468+) (Win64, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1357848230', '0', '0', 'TrinityCore rev. 2013-01-06 21:04:03 +0400 (4da2b88eed63+) (Win64, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1357991859', '0', '0', 'TrinityCore rev. 2013-01-06 21:04:03 +0400 (4da2b88eed63+) (Win64, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1357996485', '0', '0', 'TrinityCore rev. 2013-01-06 21:04:03 +0400 (4da2b88eed63+) (Win64, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1357997023', '0', '0', 'TrinityCore rev. 2013-01-06 21:04:03 +0400 (4da2b88eed63+) (Win64, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1357999170', '0', '0', 'TrinityCore rev. 2013-01-06 21:04:03 +0400 (4da2b88eed63+) (Win64, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1358023458', '0', '0', 'TrinityCore rev. 2013-01-06 21:04:03 +0400 (4da2b88eed63+) (Win64, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1358030330', '0', '0', 'TrinityCore rev. 2013-01-06 21:04:03 +0400 (4da2b88eed63+) (Win64, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1358030723', '0', '0', 'TrinityCore rev. 2013-01-06 21:04:03 +0400 (4da2b88eed63+) (Win64, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1358032218', '0', '0', 'TrinityCore rev. 2013-01-06 21:04:03 +0400 (4da2b88eed63+) (Win64, Release)');
INSERT INTO `uptime` VALUES ('1', '1358032941', '0', '0', 'TrinityCore rev. 2013-01-06 21:04:03 +0400 (4da2b88eed63+) (Win64, Release)');
INSERT INTO `uptime` VALUES ('1', '1358033329', '0', '0', 'TrinityCore rev. 2013-01-06 21:04:03 +0400 (4da2b88eed63+) (Win64, Release)');
INSERT INTO `uptime` VALUES ('1', '1358034223', '0', '0', 'TrinityCore rev. 2013-01-06 21:04:03 +0400 (4da2b88eed63+) (Win64, Release)');
INSERT INTO `uptime` VALUES ('1', '1358063298', '616', '0', 'TrinityCore rev. 2013-01-06 21:04:03 +0400 (4da2b88eed63+) (Win64, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1358102592', '618', '1', 'TrinityCore rev. 2013-01-13 22:17:00 +0400 (aab25ffbb1c8+) (Win64, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1358107621', '1201', '2', 'TrinityCore rev. 2013-01-13 22:17:00 +0400 (aab25ffbb1c8+) (Win64, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1358252390', '0', '0', 'TrinityCore rev. 2013-01-15 01:35:04 +0400 (6314dd23d299+) (Win64, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1358263570', '629', '1', 'TrinityCore rev. 2013-01-15 18:36:19 +0400 (8a04b06b2c53+) (Win64, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1358354238', '0', '0', 'TrinityCore rev. 2013-01-15 18:36:19 +0400 (8a04b06b2c53+) (Win64, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1358354417', '0', '0', 'TrinityCore rev. 2013-01-15 18:36:19 +0400 (8a04b06b2c53+) (Win64, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1358355396', '0', '0', 'TrinityCore rev. 2013-01-15 18:36:19 +0400 (8a04b06b2c53+) (Win64, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1358452914', '0', '0', 'TrinityCore rev. 2013-01-15 18:36:19 +0400 (8a04b06b2c53+) (Win64, MinSizeRel)');
INSERT INTO `uptime` VALUES ('1', '1358453523', '0', '0', 'TrinityCore rev. 2013-01-15 18:36:19 +0400 (8a04b06b2c53+) (Win64, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1358454091', '0', '0', 'TrinityCore rev. 2013-01-15 18:36:19 +0400 (8a04b06b2c53+) (Win64, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1358456570', '0', '0', 'TrinityCore rev. 2013-01-15 18:36:19 +0400 (8a04b06b2c53+) (Win64, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1358582331', '0', '0', 'TrinityCore rev. 2013-01-19 10:31:43 +0400 (b0f7766db488+) (Win64, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1358645738', '0', '0', 'TrinityCore rev. 2013-01-20 01:14:13 +0300 (874fe8a6d83e+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1358689809', '0', '0', 'TrinityCore rev. 2013-01-20 01:14:13 +0300 (874fe8a6d83e+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1358690415', '2429', '1', 'TrinityCore rev. 2013-01-20 01:14:13 +0300 (874fe8a6d83e+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1358701261', '0', '0', 'TrinityCore rev. 2013-01-20 17:59:06 +0300 (ac97812b617c+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1358701384', '601', '1', 'TrinityCore rev. 2013-01-20 17:59:06 +0300 (ac97812b617c+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1358705551', '611', '1', 'TrinityCore rev. 2013-01-20 20:48:41 +0300 (70ac5754df87+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1358971257', '1808', '1', 'TrinityCore rev. 2013-01-22 23:03:09 +0300 (4dd02690bd32) (Win32, Release)');
INSERT INTO `uptime` VALUES ('1', '1359119760', '1216', '1', 'TrinityCore rev. 2013-01-25 15:12:22 +0300 (aac44e5db4ee+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1359121274', '0', '0', 'TrinityCore rev. 2013-01-25 15:12:22 +0300 (aac44e5db4ee+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1359122440', '0', '0', 'TrinityCore rev. 2013-01-25 15:12:22 +0300 (aac44e5db4ee+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1359124210', '617', '1', 'TrinityCore rev. 2013-01-25 15:12:22 +0300 (aac44e5db4ee+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1359142190', '616', '1', 'TrinityCore rev. 2013-01-25 21:51:58 +0300 (15fb840903ef) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1359220187', '608', '1', 'TrinityCore rev. 2013-01-25 21:51:58 +0300 (15fb840903ef) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1359224232', '0', '0', 'TrinityCore rev. 2013-01-26 17:43:20 +0300 (5384c33c7069+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1359224581', '600', '1', 'TrinityCore rev. 2013-01-26 17:43:20 +0300 (5384c33c7069+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1359284415', '0', '0', 'TrinityCore rev. 2013-01-27 10:26:26 +0300 (48f26fd5a76f+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1359312383', '0', '0', 'TrinityCore rev. 2013-01-27 10:26:26 +0300 (48f26fd5a76f+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1359312415', '0', '0', 'TrinityCore rev. 2013-01-27 10:26:26 +0300 (48f26fd5a76f+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1359318471', '57609', '0', 'TrinityCore rev. 2013-01-27 22:00:55 +0300 (c6018dceccfe) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1359667217', '3024', '0', 'TrinityCore rev. 2013-01-30 22:51:25 +0300 (b329204765f2+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1359830041', '0', '0', 'TrinityCore rev. 2013-02-02 20:41:54 +0300 (ffe836598ee9) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1359831966', '1201', '0', 'TrinityCore rev. 2013-02-02 20:41:54 +0300 (ffe836598ee9) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1359837933', '0', '0', 'TrinityCore rev. 2013-02-02 20:41:54 +0300 (ffe836598ee9) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1359838304', '0', '0', 'TrinityCore rev. 2013-02-02 20:41:54 +0300 (ffe836598ee9) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1359838582', '0', '0', 'TrinityCore rev. 2013-02-02 20:41:54 +0300 (ffe836598ee9) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1359838940', '3601', '2', 'TrinityCore rev. 2013-02-02 20:41:54 +0300 (ffe836598ee9) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1359869498', '15601', '0', 'TrinityCore rev. 2013-02-02 20:41:54 +0300 (ffe836598ee9) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1359887052', '33002', '1', 'TrinityCore rev. 2013-02-02 20:41:54 +0300 (ffe836598ee9) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1360001267', '0', '0', 'TrinityCore rev. 2013-02-04 19:17:35 +0300 (59a7d3bebd26+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1360002606', '0', '0', 'TrinityCore rev. 2013-02-04 19:17:35 +0300 (59a7d3bebd26+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1360087203', '10813', '3', 'TrinityCore rev. 2013-02-04 19:17:35 +0300 (59a7d3bebd26+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1360151159', '3609', '3', 'TrinityCore rev. 2013-02-04 19:17:35 +0300 (59a7d3bebd26+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1360155031', '0', '0', 'TrinityCore rev. 2013-02-04 19:17:35 +0300 (59a7d3bebd26+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1360155322', '18600', '3', 'TrinityCore rev. 2013-02-04 19:17:35 +0300 (59a7d3bebd26+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1360174166', '0', '0', 'TrinityCore rev. 2013-02-04 19:17:35 +0300 (59a7d3bebd26+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1360177010', '0', '0', 'TrinityCore rev. 2013-02-04 19:17:35 +0300 (59a7d3bebd26+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1360181416', '1809', '0', 'TrinityCore rev. 2013-02-04 19:17:35 +0300 (59a7d3bebd26+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1360238657', '15601', '1', 'TrinityCore rev. 2013-02-04 19:17:35 +0300 (59a7d3bebd26+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1360254896', '608', '0', 'TrinityCore rev. 2013-02-04 19:17:35 +0300 (59a7d3bebd26+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1360255536', '2401', '1', 'TrinityCore rev. 2013-02-04 19:17:35 +0300 (59a7d3bebd26+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1360258621', '5401', '1', 'TrinityCore rev. 2013-02-04 19:17:35 +0300 (59a7d3bebd26+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1360264602', '2406', '1', 'TrinityCore rev. 2013-02-04 19:17:35 +0300 (59a7d3bebd26+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1360267496', '604', '1', 'TrinityCore rev. 2013-02-04 19:17:35 +0300 (59a7d3bebd26+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1360268336', '1802', '1', 'TrinityCore rev. 2013-02-04 19:17:35 +0300 (59a7d3bebd26+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1360333681', '19207', '0', 'TrinityCore rev. 2013-02-04 19:17:35 +0300 (59a7d3bebd26+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1360361260', '55224', '1', 'TrinityCore rev. 2013-02-08 23:57:12 +0300 (30702fb0d557+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1360438955', '609', '0', 'TrinityCore rev. 2013-02-09 01:03:47 +0300 (11a5cdfa748e) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1360439697', '0', '0', 'TrinityCore rev. 2013-02-09 01:03:47 +0300 (11a5cdfa748e) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1360522148', '1214', '1', 'TrinityCore rev. 2013-02-10 00:21:06 +0300 (eefb0e6ec857+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1360523849', '0', '0', 'TrinityCore rev. 2013-02-10 00:21:06 +0300 (eefb0e6ec857+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1360524285', '0', '0', 'TrinityCore rev. 2013-02-10 00:21:06 +0300 (eefb0e6ec857+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1360524458', '0', '0', 'TrinityCore rev. 2013-02-10 00:21:06 +0300 (eefb0e6ec857+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1360524747', '1801', '1', 'TrinityCore rev. 2013-02-10 00:21:06 +0300 (eefb0e6ec857+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1360527357', '601', '1', 'TrinityCore rev. 2013-02-10 00:21:06 +0300 (eefb0e6ec857+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1360529082', '608', '1', 'TrinityCore rev. 2013-02-10 21:47:10 +0300 (6a5617ba90bf+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1360605444', '0', '0', 'TrinityCore rev. 2013-02-10 23:57:24 +0300 (017f527343b5) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1360614103', '0', '0', 'TrinityCore rev. 2013-02-10 23:57:24 +0300 (017f527343b5) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1360615530', '612', '1', 'TrinityCore rev. 2013-02-10 23:57:24 +0300 (017f527343b5+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1360616485', '0', '0', 'TrinityCore rev. 2013-02-10 23:57:24 +0300 (017f527343b5+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1360674633', '0', '0', 'TrinityCore rev. 2013-02-10 23:57:24 +0300 (017f527343b5+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1360675798', '0', '0', 'TrinityCore rev. 2013-02-10 23:57:24 +0300 (017f527343b5+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1360675975', '0', '0', 'TrinityCore rev. 2013-02-10 23:57:24 +0300 (017f527343b5+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1360676398', '0', '0', 'TrinityCore rev. 2013-02-10 23:57:24 +0300 (017f527343b5+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1360683011', '4813', '1', 'TrinityCore rev. 2013-02-10 23:57:24 +0300 (017f527343b5+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1360758582', '0', '0', 'TrinityCore rev. 2013-02-10 23:57:24 +0300 (017f527343b5+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1360759153', '1200', '1', 'TrinityCore rev. 2013-02-10 23:57:24 +0300 (017f527343b5+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1360776509', '615', '1', 'TrinityCore rev. 2013-02-13 15:53:31 +0300 (71c528d6d624+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1360792080', '0', '0', 'TrinityCore rev. 2013-02-13 15:53:31 +0300 (71c528d6d624+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1360792602', '0', '0', 'TrinityCore rev. 2013-02-13 15:53:31 +0300 (71c528d6d624+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1360869850', '601', '1', 'TrinityCore rev. 2013-02-13 15:53:31 +0300 (71c528d6d624+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1360943270', '0', '0', 'TrinityCore rev. 2013-02-14 17:56:08 +0100 (7f0149e2d1c1+) (Win32, Release)');
INSERT INTO `uptime` VALUES ('1', '1360943604', '0', '0', 'TrinityCore rev. 2013-02-14 17:56:08 +0100 (7f0149e2d1c1+) (Win32, Release)');
INSERT INTO `uptime` VALUES ('1', '1360944216', '608', '1', 'TrinityCore rev. 2013-02-13 15:53:31 +0300 (71c528d6d624+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1360944934', '0', '0', 'TrinityCore rev. 2013-02-13 15:53:31 +0300 (71c528d6d624+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1360954353', '0', '0', 'TrinityCore rev. 2013-02-13 15:53:31 +0300 (71c528d6d624+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1360955491', '0', '0', 'TrinityCore rev. 2013-02-13 15:53:31 +0300 (71c528d6d624+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1360956122', '0', '0', 'TrinityCore rev. 2013-02-14 17:56:08 +0100 (7f0149e2d1c1+) (Win32, Release)');
INSERT INTO `uptime` VALUES ('1', '1360963642', '0', '0', 'TrinityCore rev. 2013-02-13 15:53:31 +0300 (71c528d6d624+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1361209435', '2415', '0', 'TrinityCore rev. 2013-02-18 19:44:52 +0300 (3660e6973e8d) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1361215310', '0', '0', 'TrinityCore rev. 2013-02-18 19:44:52 +0300 (3660e6973e8d) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1361543711', '0', '0', 'TrinityCore rev. 2013-02-18 19:44:52 +0300 (3660e6973e8d) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1361544308', '0', '0', 'TrinityCore rev. 2013-02-18 19:44:52 +0300 (3660e6973e8d) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1361565385', '0', '0', 'TrinityCore rev. 2013-02-18 19:44:52 +0300 (3660e6973e8d) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1361565521', '1200', '1', 'TrinityCore rev. 2013-02-18 19:44:52 +0300 (3660e6973e8d) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1361567185', '601', '1', 'TrinityCore rev. 2013-02-18 19:44:52 +0300 (3660e6973e8d) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1361567920', '0', '0', 'TrinityCore rev. 2013-02-18 19:44:52 +0300 (3660e6973e8d) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1361604632', '0', '0', 'TrinityCore rev. 2013-02-18 19:44:52 +0300 (3660e6973e8d) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1361622279', '0', '0', 'TrinityCore rev. 2013-02-18 19:44:52 +0300 (3660e6973e8d) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1361622965', '4200', '1', 'TrinityCore rev. 2013-02-18 19:44:52 +0300 (3660e6973e8d) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1361636610', '0', '0', 'TrinityCore rev. 2013-02-23 18:03:18 +0300 (4512c625465e+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1361636858', '0', '0', 'TrinityCore rev. 2013-02-23 18:03:18 +0300 (4512c625465e+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1361637850', '0', '0', 'TrinityCore rev. 2013-02-23 18:03:18 +0300 (4512c625465e+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1361638559', '0', '0', 'TrinityCore rev. 2013-02-23 18:03:18 +0300 (4512c625465e+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1361638865', '0', '0', 'TrinityCore rev. 2013-02-23 18:03:18 +0300 (4512c625465e+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1361640948', '0', '0', 'TrinityCore rev. 2013-02-23 18:03:18 +0300 (4512c625465e+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1361641797', '0', '0', 'TrinityCore rev. 2013-02-23 18:03:18 +0300 (4512c625465e+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1361730239', '0', '0', 'TrinityCore rev. 2013-02-23 18:03:18 +0300 (4512c625465e+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1361730445', '0', '0', 'TrinityCore rev. 2013-02-23 18:03:18 +0300 (4512c625465e+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1361730728', '0', '0', 'TrinityCore rev. 2013-02-23 18:03:18 +0300 (4512c625465e+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1361730925', '0', '0', 'TrinityCore rev. 2013-02-23 18:03:18 +0300 (4512c625465e+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1361798832', '0', '0', 'TrinityCore rev. 2013-02-24 21:21:15 +0300 (9eb96ca4b2a4) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1361798953', '601', '1', 'TrinityCore rev. 2013-02-24 21:21:15 +0300 (9eb96ca4b2a4) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1361804989', '1201', '1', 'TrinityCore rev. 2013-02-24 21:21:15 +0300 (9eb96ca4b2a4) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1361810249', '0', '0', 'TrinityCore rev. 2013-02-25 18:42:08 +0300 (e3a54e615cf9) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1361810381', '4201', '0', 'TrinityCore rev. 2013-02-25 18:42:08 +0300 (e3a54e615cf9) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1361971624', '0', '0', 'TrinityCore rev. 2013-02-27 15:34:01 +0300 (70243b506ddf+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1361986237', '619', '1', 'TrinityCore rev. 2013-02-27 15:34:01 +0300 (70243b506ddf+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1361988104', '0', '0', 'TrinityCore rev. 2013-02-27 15:34:01 +0300 (70243b506ddf+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1362077595', '614', '0', 'TrinityCore rev. 2013-02-27 15:34:01 +0300 (70243b506ddf+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1362078359', '3001', '1', 'TrinityCore rev. 2013-02-27 15:34:01 +0300 (70243b506ddf+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1362134415', '0', '0', 'TrinityCore rev. 2013-02-27 15:34:01 +0300 (70243b506ddf+) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1362300544', '0', '0', 'TrinityCore rev. 4eed1e5c512e 2013-03-01 13:38:26 +0300 (master branch) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1362324696', '626', '1', 'TrinityCore rev. 4eed1e5c512e+ 2013-03-01 13:38:26 +0300 (master branch) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1362326572', '1210', '0', 'TrinityCore rev. 1346def52fd2 2013-03-03 18:52:29 +0300 (master branch) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1362329800', '600', '0', 'TrinityCore rev. 1346def52fd2 2013-03-03 18:52:29 +0300 (master branch) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1362340960', '616', '1', 'TrinityCore rev. 1346def52fd2+ 2013-03-03 18:52:29 +0300 (master branch) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1362343911', '0', '0', 'TrinityCore rev. 1346def52fd2+ 2013-03-03 18:52:29 +0300 (master branch) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1362343962', '0', '0', 'TrinityCore rev. 1346def52fd2+ 2013-03-03 18:52:29 +0300 (master branch) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1362344053', '0', '0', 'TrinityCore rev. 1346def52fd2+ 2013-03-03 18:52:29 +0300 (master branch) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1362344783', '0', '0', 'TrinityCore rev. 1346def52fd2+ 2013-03-03 18:52:29 +0300 (master branch) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1362598813', '619', '0', 'TrinityCore rev. 166b3f776e38 2013-03-04 22:49:07 +0300 (master branch) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1362599941', '0', '0', 'TrinityCore rev. 166b3f776e38 2013-03-04 22:49:07 +0300 (master branch) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1362600125', '0', '0', 'TrinityCore rev. 166b3f776e38 2013-03-04 22:49:07 +0300 (master branch) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1362600260', '0', '0', 'TrinityCore rev. 166b3f776e38 2013-03-04 22:49:07 +0300 (master branch) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1362600596', '0', '0', 'TrinityCore rev. 166b3f776e38 2013-03-04 22:49:07 +0300 (master branch) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1362603412', '0', '0', 'TrinityCore rev. 166b3f776e38 2013-03-04 22:49:07 +0300 (master branch) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1362605013', '0', '0', 'TrinityCore rev. 166b3f776e38 2013-03-04 22:49:07 +0300 (master branch) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1362605110', '4200', '0', 'TrinityCore rev. 166b3f776e38 2013-03-04 22:49:07 +0300 (master branch) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1362682500', '0', '0', 'TrinityCore rev. 7d6fabe8822d 2013-03-07 21:23:16 +0300 (master branch) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1362683232', '0', '0', 'TrinityCore rev. 7d6fabe8822d 2013-03-07 21:23:16 +0300 (master branch) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1362749996', '0', '0', 'TrinityCore rev. 06018ea6fc0f 2013-03-08 09:25:32 +0300 (master branch) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1362759364', '0', '0', 'TrinityCore rev. 06018ea6fc0f 2013-03-08 09:25:32 +0300 (master branch) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1362819536', '0', '0', 'TrinityCore rev. 7014ce71b94e 2013-03-09 11:46:40 +0300 (master branch) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1363107548', '0', '0', 'TrinityCore rev. 3f6714ac36b3 2013-03-12 01:29:42 +0300 (master branch) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1363112613', '1212', '0', 'TrinityCore rev. 3f6714ac36b3 2013-03-12 01:29:42 +0300 (master branch) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1363116921', '610', '1', 'TrinityCore rev. 3f6714ac36b3 2013-03-12 01:29:42 +0300 (master branch) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1363117650', '0', '0', 'TrinityCore rev. 3f6714ac36b3 2013-03-12 01:29:42 +0300 (master branch) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1363117857', '0', '0', 'TrinityCore rev. 3f6714ac36b3 2013-03-12 01:29:42 +0300 (master branch) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1363638405', '0', '0', 'TrinityCore rev. 6c4b34cc5200 2013-03-18 13:39:28 +0300 (master branch) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1363710671', '606', '0', 'TrinityCore rev. 6c4b34cc5200 2013-03-18 13:39:28 +0300 (master branch) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1363756259', '0', '0', 'TrinityCore rev. 6c4b34cc5200 2013-03-18 13:39:28 +0300 (master branch) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1363891246', '1215', '1', 'TrinityCore rev. 6c4b34cc5200 2013-03-18 13:39:28 +0300 (master branch) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1364139122', '0', '0', 'TrinityCore rev. 765fe5682a39+ 2013-03-23 01:01:06 +0300 (master branch) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1364240379', '0', '0', 'TrinityCore rev. 74028a531a4b+ 2013-03-25 21:45:27 +0300 (master branch) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1364241430', '0', '0', 'TrinityCore rev. 74028a531a4b+ 2013-03-25 21:45:27 +0300 (master branch) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1364575598', '0', '0', 'TrinityCore rev. b0604616cbef+ 2013-03-27 18:32:08 +0300 (master branch) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1364630260', '0', '0', 'TrinityCore rev. a988ab6c9b01+ 2013-03-29 19:28:04 +0300 (master branch) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1364630369', '0', '0', 'TrinityCore rev. a988ab6c9b01+ 2013-03-29 19:28:04 +0300 (master branch) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1364748235', '0', '0', 'TrinityCore rev. c8614e90731d 2013-03-31 12:46:21 +0400 (master branch) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1364752809', '0', '0', 'TrinityCore rev. c8614e90731d 2013-03-31 12:46:21 +0400 (master branch) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1364843223', '613', '0', 'TrinityCore rev. c8614e90731d 2013-03-31 12:46:21 +0400 (master branch) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1364844147', '4201', '1', 'TrinityCore rev. c8614e90731d 2013-03-31 12:46:21 +0400 (master branch) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1365097887', '0', '0', 'TrinityCore rev. 26b9be25b32d 2013-04-04 21:26:57 +0400 (master branch) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1365154009', '0', '0', 'TrinityCore rev. 26b9be25b32d 2013-04-04 21:26:57 +0400 (master branch) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1365180876', '1816', '2', 'TrinityCore rev. 1211ea4fe535 2013-04-05 13:32:49 +0400 (master branch) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1365188398', '1207', '1', 'TrinityCore rev. 1211ea4fe535 2013-04-05 13:32:49 +0400 (master branch) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1365236279', '0', '0', 'TrinityCore rev. 1211ea4fe535 2013-04-05 13:32:49 +0400 (master branch) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1365236991', '0', '0', 'TrinityCore rev. 1211ea4fe535 2013-04-05 13:32:49 +0400 (master branch) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1365237198', '2402', '1', 'TrinityCore rev. 1211ea4fe535 2013-04-05 13:32:49 +0400 (master branch) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1365247320', '610', '1', 'TrinityCore rev. 1211ea4fe535 2013-04-05 13:32:49 +0400 (master branch) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1365263551', '0', '0', 'TrinityCore rev. 1211ea4fe535 2013-04-05 13:32:49 +0400 (master branch) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1365264585', '4210', '0', 'TrinityCore rev. 1211ea4fe535 2013-04-05 13:32:49 +0400 (master branch) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1365272730', '0', '0', 'TrinityCore rev. 1211ea4fe535 2013-04-05 13:32:49 +0400 (master branch) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1365275860', '3012', '1', 'TrinityCore rev. b52206520654+ 2013-04-06 12:26:54 +0400 (master branch) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1365326367', '0', '0', 'TrinityCore rev. b52206520654+ 2013-04-06 12:26:54 +0400 (master branch) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1365326782', '600', '1', 'TrinityCore rev. b52206520654+ 2013-04-06 12:26:54 +0400 (master branch) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1365439745', '0', '0', 'TrinityCore rev. b52206520654+ 2013-04-06 12:26:54 +0400 (master branch) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1365447074', '0', '0', 'TrinityCore rev. b52206520654+ 2013-04-06 12:26:54 +0400 (master branch) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1365448134', '0', '0', 'TrinityCore rev. b52206520654+ 2013-04-06 12:26:54 +0400 (master branch) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1365451031', '0', '0', 'TrinityCore rev. c9fd1c08c305 2013-04-08 23:35:10 +0400 (master branch) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1365451209', '1201', '1', 'TrinityCore rev. c9fd1c08c305 2013-04-08 23:35:10 +0400 (master branch) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1365452553', '0', '0', 'TrinityCore rev. c9fd1c08c305 2013-04-08 23:35:10 +0400 (master branch) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('1', '1365535228', '0', '0', 'TrinityCore rev. 04f619fec60c+ 2013-04-09 22:17:03 +0400 (mmaps branch) (Win32, Release)');
INSERT INTO `uptime` VALUES ('1', '1365536519', '0', '0', 'TrinityCore rev. 04f619fec60c+ 2013-04-09 22:17:03 +0400 (mmaps branch) (Win32, Release)');
INSERT INTO `uptime` VALUES ('1', '1365536617', '0', '0', 'TrinityCore rev. 04f619fec60c+ 2013-04-09 22:17:03 +0400 (mmaps branch) (Win32, Release)');
INSERT INTO `uptime` VALUES ('1', '1365536997', '0', '0', 'TrinityCore rev. 04f619fec60c+ 2013-04-09 22:17:03 +0400 (mmaps branch) (Win32, Release)');
INSERT INTO `uptime` VALUES ('1', '1365537165', '0', '0', 'TrinityCore rev. 04f619fec60c+ 2013-04-09 22:17:03 +0400 (mmaps branch) (Win32, Release)');
INSERT INTO `uptime` VALUES ('1', '1365537437', '0', '0', 'TrinityCore rev. 04f619fec60c+ 2013-04-09 22:17:03 +0400 (mmaps branch) (Win32, Release)');
INSERT INTO `uptime` VALUES ('1', '1365538005', '0', '0', 'TrinityCore rev. 04f619fec60c+ 2013-04-09 22:17:03 +0400 (mmaps branch) (Win32, Release)');
INSERT INTO `uptime` VALUES ('1', '1365538096', '0', '0', 'TrinityCore rev. c9fd1c08c305 2013-04-08 23:35:10 +0400 (master branch) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('2', '1354907969', '0', '0', 'TrinityCore rev. 2012-12-06 09:38:39 +0400 (5876d741a84b) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('2', '1355856084', '0', '0', 'TrinityCore rev. 2012-12-06 09:38:39 +0400 (5876d741a84b) (Win32, RelWithDebInfo)');
INSERT INTO `uptime` VALUES ('2', '1356046274', '0', '0', 'TrinityCore rev. 2012-12-20 02:03:53 +0400 (e0a4ea4062e9+) (Win32, MinSizeRel)');
INSERT INTO `uptime` VALUES ('2', '1356046317', '0', '0', 'TrinityCore rev. 2012-12-20 02:03:53 +0400 (e0a4ea4062e9+) (Win32, MinSizeRel)');
INSERT INTO `uptime` VALUES ('2', '1356262612', '0', '0', 'TrinityCore rev. 2012-12-20 02:03:53 +0400 (e0a4ea4062e9+) (Win32, MinSizeRel)');
INSERT INTO `uptime` VALUES ('2', '1356262752', '0', '0', 'TrinityCore rev. 2012-12-20 02:03:53 +0400 (e0a4ea4062e9+) (Win32, MinSizeRel)');
INSERT INTO `uptime` VALUES ('2', '1356712536', '5074', '0', 'TrinityCore rev. 2012-12-22 17:01:55 +0400 (3bffdbde5eb6+) (Win32, MinSizeRel)');
INSERT INTO `uptime` VALUES ('2', '1356719961', '0', '0', 'TrinityCore rev. 2012-12-22 17:01:55 +0400 (3bffdbde5eb6+) (Win32, MinSizeRel)');
INSERT INTO `uptime` VALUES ('2', '1356720080', '0', '0', 'TrinityCore rev. 2012-12-22 17:01:55 +0400 (3bffdbde5eb6+) (Win32, MinSizeRel)');
